<?
class CPL_Admin_Modules_WebBasic_Section_View extends CP_Admin_Modules_WebBasic_Section_View
{
    function getList($dataArray){
        $listObj = Zend_Registry::get('listObj');
        $fn = Zend_Registry::get('fn');
        $cpCfg = Zend_Registry::get('cpCfg');

        $rows  = "";
        $rowCounter = 0;

        $fnModCountry = includeCPClass('ModuleFns', 'common_country');

        //--------------------------------------------------------------------------//
        foreach ($dataArray as $row){
            $refTitle ='';
            if ($cpCfg['m.webBasic.section.showRefTitle']){
                $refTitle = $listObj->getListDataCell($row['title_ref']);
            }
            
            $rows .= "
            {$listObj->getListRowHeader($row, $rowCounter)}
            {$listObj->getGoToDetailText($rowCounter, $row['title'])}
            {$refTitle}
            {$fnModCountry->getCountryValueCellInList($row)}
            {$listObj->getListSortOrderField($row, 'section_id')}
            {$listObj->getListDataCell($row['section_type'])}
            {$listObj->getListDataCell($row['button_position'])}
            {$fn->getSiteFldForList($row)}
            {$listObj->getListDataCell($row['section_id'], 'center')}
            {$listObj->getListPublishedImage($row['published'], $row['section_id'])}
            {$listObj->getListRowEnd($row['section_id'])}
            ";
            $rowCounter++ ;
        }

        $refTitle ='';
        if ($cpCfg['m.webBasic.section.showRefTitle']){
            $refTitle = $listObj->getListHeaderCell('Ref. Title', 's.title_ref');
        }

        $text = "
        {$listObj->getListHeader()}
        {$listObj->getListHeaderCell('Title', 's.title')}
        {$refTitle}
        {$fnModCountry->getCountryLabelCellInList()}
        {$listObj->getListSortOrderImage('s')}
        {$listObj->getListHeaderCell('Section Type', 's.section_type')}
        {$listObj->getListHeaderCell('Button Position', 's.button_position')}
        {$fn->getSiteLabelForList()}
        {$listObj->getListHeaderCell('ID', 's.section_id', 'headerCenter')}
        {$listObj->getListHeaderCell('Published', 's.published', 'headerCenter')}
        {$listObj->getListHeaderEnd()}
        {$rows}
        {$listObj->getListFooter()}
        ";

        return $text;
    }

    /**
     *
     */
    function getEdit($row) {
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $cpCfg = Zend_Registry::get('cpCfg');
        $formObj = Zend_Registry::get('formObj');

        $fieldset1 = "
        {$formObj->getTBRow('Title', 'title', $ln->gfv($row, 'title', '0'))}
        {$formObj->getDDRowByArr('Section Type', 'section_type', $cpCfg['m.webBasic.section.recordTypeArr'], $row['section_type'])}
        {$formObj->getDDRowByArr('Button Position', 'button_position', $cpCfg['m.webBasic.section.btnPosArr'], $row['button_position'])}
        {$formObj->getTBRow('External Link', 'external_link', $row['external_link'])}
        {$formObj->getTBRow('Internal Link', 'internal_link', $row['internal_link'])}
        {$formObj->getTBRow('CSS Style Name', 'css_style_name', $row['css_style_name'])}
        {$formObj->getYesNoRRow('Published', 'published', $row['published'])}
        {$formObj->getYesNoRRow('Show in the Navigation', 'show_in_nav', $row['show_in_nav'])}
       
		";

        $fieldset2 = "
        {$formObj->getYesNoRRow('Public', 'access_to_public', $row['access_to_public'])}
        {$formObj->getYesNoRRow('Student', 'access_to_student', $row['access_to_student'])}
        {$formObj->getYesNoRRow('Parent', 'access_to_parent', $row['access_to_parent'])}
        {$formObj->getYesNoRRow('Teacher', 'access_to_teacher', $row['access_to_teacher'])}
		";

        $text = "
        {$formObj->getFieldSetWrapped('Section Details', $fieldset1)}
        {$formObj->getFieldSetWrapped('Access To', $fieldset2)}
        {$formObj->getMetaData($row)}
        {$formObj->getCreationModificationText($row)}
        ";

        return $text;
    }

}