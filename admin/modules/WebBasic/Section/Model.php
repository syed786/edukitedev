<?
class CPL_Admin_Modules_WebBasic_Section_Model extends CP_Admin_Modules_WebBasic_Section_Model
{
    /**
     *
     */
    function getFields() {
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $tv = Zend_Registry::get('tv');
        $cpUrl = Zend_Registry::get('cpUrl');

        $fa = parent::getFields();

        $fa = $fn->addToFieldsArray($fa, 'access_to_student');
        $fa = $fn->addToFieldsArray($fa, 'access_to_teacher');
        $fa = $fn->addToFieldsArray($fa, 'access_to_parent');
        $fa = $fn->addToFieldsArray($fa, 'access_to_public');

        return $fa;
    }
}
