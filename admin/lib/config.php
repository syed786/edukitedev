<?
$cpCfg = array();

$cpCfg['cp.topRooms'] = array(
    'main' => array(
        'title' => 'Main'
       ,'modules' => array(
             'edukite_student'
            ,'edukite_parent'
            ,'edukite_teacher'
            ,'edukite_class'
            ,'edukite_subject'
            ,'edukite_task'
            ,'edukite_yearGroup'
            ,'edukite_notice'
            ,'edukite_achievement'
       )
       ,'default' => 'edukite_student'
    )

    ,'cms' => array(
        'title' => 'CMS'
       ,'modules' => array(
             'webBasic_content'
            ,'common_contact'
            ,'webBasic_enquiry'
            ,'common_interest'
            ,'common_broadcast'
       )
       ,'default' => 'webBasic_content'
    )
    ,'admin' => array(
        'title' => 'Admin'
       ,'modules' => array(
             'webBasic_section'
            ,'webBasic_category'
            ,'webBasic_subCategory'
            ,'core_staff'
            ,'core_valuelist'
            ,'core_setting'
            ,'core_translation'
            ,'core_adminTranslation'
            ,'common_geoCountry'
       )
       ,'default' => 'core_translation'
    )
);

$hiddenModules = array(
     'common_contactLink'
    ,'common_testRecipientLink'
    ,'common_interestLink'
    ,'edukite_parentLink'
    ,'edukite_classLink'
    ,'edukite_studentLink'
    ,'edukite_teacherLink'
);

$tmpName = &$cpCfg['cp.topRooms'];
$cpCfg['cp.availableModules'] = array_merge(
     $tmpName['main']['modules']
    ,$tmpName['cms']['modules']
    ,$tmpName['admin']['modules']
    ,$hiddenModules
);

$cpCfg['cp.availableModGroups'] = array(
     'core'
    ,'common'
    ,'webBasic'
);

$cpCfg['cp.availableWidgets'] = array(
);

$cpCfg['cp.availablePlugins'] = array(
     'common_comment'
    ,'common_media'
    ,'common_login'
);

return $cpCfg;