<?
$cpCfg = array();

$cpCfg['m.webBasic.section.hasBanner'] = 1;
$cpCfg['m.webBasic.section.hasCSSStyleName'] = 1;
$cpCfg['m.edukite.current_academic_year']  = date('Y');

$cpCfg['m.webBasic.section.recordTypeArr'] = array (
     'Content'
    ,'Home'
    ,'Site Search'
    ,'Enquiry Form'
    ,'Contact Us'
    ,'============='
    ,'Login'
    ,'Business Login'
    ,'Register'
    ,'Claim Business'
    ,'Reset Password'
    ,'============='
    ,'Dashboard'
    ,'Student'
    ,'Teacher'
    ,'Parent'
    ,'Class'
    ,'Subject'
    ,'Year Group'
    ,'Interest'
    ,'Type'
    ,'Achievement'
    ,'Daily Activity'
    ,'============='
    ,'Staff Notice'
    ,'Notice'
    ,'Task'
    ,'Portfolio'
    ,'Calendar'
    ,'Kite Notice'
);

$cpCfg['m.webBasic.category.recordTypeArr'] = array (
     'Content'
    ,'Enquiry Form'
);

$cpCfg['m.webBasic.section.btnPosArr'] = array (
     'Top Most'
    ,'Top'
    ,'Left'
    ,'Bottom'
    ,'Other'
);

$cpCfg['m.webBasic.content.recordTypeArr'] = array(
     'Record'
    ,'Callout'
    ,'Social Media Icons'
    ,'Homepage Call-to-Action'
    ,'Disclaimer'
    ,'Privacy Policy'
    ,'Left Column For Student'
    ,'Middle Column For Student'
    ,'Right Column For Student'
);

$cpCfg['m.core.valuelist.recordTypeArr'] = array(
     'enquiryStatus' => 'Enquiry Status'
    ,'enquiryClientType' => 'Enquiry Client Type'
    ,'salutation' => 'Salutation'
    ,'businessType' => 'Business Type'
    ,'businessStatus' => 'Business Status'
    ,'dressType' => 'Dress Type'
    ,'parkingType' => 'Parking Type'
    ,'ambienceType' => 'Ambience Type'
    ,'topLevelDomain' => 'Top Level Domain'
    ,'ageGroup' => 'Age Group'
);
return $cpCfg;