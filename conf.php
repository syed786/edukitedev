<?
$config['cp.TimeZone'] = 'Australia/Sydney';
date_default_timezone_set($config['cp.TimeZone']);

define('CP_HOST', $_SERVER['HTTP_HOST']);

//================================================================//
if (CP_HOST == "edukitedev.edukite.com" || CP_HOST == "www.edukitedev.edukite.com") {
    define('CP_ENV', 'production');
    define('CP_CORE_PATH', '/home/cmsPilot/v3.0/');

} else if (CP_HOST == "edukitedev.testpilotweb.com") {
    define('CP_ENV', 'testing');
    define('CP_CORE_PATH', '/www-disk/inetpub/Apache/cmsPilot/v3.0/');

} else if (CP_HOST == "edukitedev.testpilotweb3.com") {
    define('CP_ENV', 'development');
    define('CP_CORE_PATH', '/var/www/vhosts/cmsPilot/v3.0/');

} else if (CP_HOST == "edukitedev.usoftdev.com") {
    define('CP_ENV', 'testing');
    define('CP_CORE_PATH', '/home/cmsPilot/v3.0/');

} else if (CP_HOST == "edukitedev.usstestserver.com") {
    define('CP_ENV', 'testing');
    define('CP_CORE_PATH', '/home/cmsPilot/v3.0-test/');

} else if (CP_HOST == "edukitedev.localhost") {
    $docRoot = $_SERVER['DOCUMENT_ROOT'];
    $rootFolder = substr($docRoot, 0, stripos($docRoot, '/edukitedev/'));

    define('CP_ENV', 'local');
    define('CP_CORE_PATH', $rootFolder . '/cmsPilot/v3.0/');
}

define('CP_PATH', CP_CORE_PATH . 'CP/');
define('CP_PATH2', CP_CORE_PATH . 'CP2/');
//================================================================//
require_once(CP_PATH . 'common/lib/inc_path.php');

/*** Local Server **/
$config['local'] = array(
     'db' => array(
          'host'     => 'localhost'
         ,'username' => 'root'
         ,'password' => $_SERVER['dbPassword']
         ,'dbname'   => 'edukitedev'
     )
    ,'display_errors' => true
);

/*** Development Server **/
$config['development'] = $config['local'];
$config['development']['db']['username'] = 'edukitedev';
$config['development']['db']['password'] = 'edu12ki34te56dev';

/*** Testing Server **/
/*** Testing Server **/
$config['testing'] = $config['development'];
$config['production']['db']['dbname'] = 'edukitedev';
$config['production']['db']['username'] = 'edukitedev';
$config['production']['db']['password'] = 'edu12ki34te56dev';
$config['testing']['display_errors'] = false;

/*** Production Server **/
$config['production'] = $config['testing'];
$config['production']['display_errors'] = false;
$config['production']['db']['dbname'] = 'edukited_dev';
$config['production']['db']['username'] = 'edukited_dev';
$config['production']['db']['password'] = 'zzUQHZD})~Mk';

$config['production']['SMTPServer']   =  'retail.smtp.com';
$config['production']['SMTPPort']     =  2525;
$config['production']['SMTPUsername'] =  'autonotification2@kitesonacloud.com';
$config['production']['SMTPPassword'] =  'Auto123$';


//================================================================//
require_once(CP_PATH . 'common/lib/Registry.php');
$cfgCommon = require_once(CP_PATH . 'common/lib/config.php');
$cfgCommon2 = require_once(CP_PATH2 . 'common/lib/config.php');
$cfgUser = require_once($cfgCommon['cp.configWWWFilePath']);
$cfgUser = (is_array($cfgUser)) ? $cfgUser : array();
$cfgMast = require_once($cfgCommon['cp.masterPath'] . 'lib/config.php');
$cfgLoc  = require_once($cfgCommon['cp.localPath'] . 'lib/config.php');

$cpCfg = array_merge($config, $cfgCommon, $cfgMast, $cfgLoc);
Zend_Registry::set('cpCfg',$cpCfg);
//================================================================//
