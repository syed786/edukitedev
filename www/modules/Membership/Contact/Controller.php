<?
class CPL_Www_Modules_Membership_Contact_Controller extends CP_Www_Modules_Membership_Contact_Controller
{
    //==================================================================//
    function getController() {
        $tv = Zend_Registry::get('tv');
        $ln = Zend_Registry::get('ln');
        $cpUrl = Zend_Registry::get('cpUrl');
        $cpUtil = Zend_Registry::get('cpUtil');
        $cpCfg = Zend_Registry::get('cpCfg');
        $formObj = Zend_Registry::get('formObj');
        if (!isLoggedInWWW() && ($tv['secType'] == 'Login'
            || $tv['catType'] == 'Login'
            || $tv['subCatType'] == 'Login')
            ){
            $wLogin = getCPWidgetObj('member_loginForm');

            $typeArr = array(
                 'edukite_teacher' => 'Administrator'
                ,'edukite_student' => 'Student'
                ,'edukite_parent'  => 'Parent'
            );

            return $wLogin->getWidget(array(
                 'hasRegiserInfo' => false
                ,'loginTypeArr' => $typeArr
                ,'hasForgotPass' => false
            ));
        } else if ($tv['secType'] == 'Logout'
                || $tv['catType'] == 'Logout'
                || $tv['subCatType'] == 'Logout'
                ) {
            $pLogin = getCPPluginObj('member_login');
            return $pLogin->model->getLogout();


        } else if (isLoggedInWWW() && $tv['secType'] == 'Login'){
            $homeUrl = $cpUrl->getUrlBySecType('Home');
            $cpUtil->redirect($homeUrl);
        } else if ($tv['secType'] == 'Reset Password'){
            $pReset = getCPPluginObj('member_resetPassword');
            return $pReset->view->getResetPasswordForm();
        } else {
            checkLoggedIn();
        }

    }
}