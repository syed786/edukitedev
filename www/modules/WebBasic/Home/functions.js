Util.createCPObject('cpm.webBasic.home');

cpm.webBasic.home = {
    init: function(){
        $(window).load(function(){
        });

        $(".homeMiddle .youTube iframe").attr("width","390");
        $(".homeMiddle .youTube iframe").attr("height","290");

        $(".homeLeft .youTube iframe").attr("width","287");
        $(".homeLeft .youTube iframe").attr("height","200");

        $('.controller img').hover(function(){
            $(this).attr('src','/www/themes/Kite/images/controller-active.gif');
        },function(){
            $(this).attr('src','/www/themes/Kite/images/controller.gif');
        });

        $(".readNotice input[type=radio]").livequery('click', function(){
            var parent = $(this).closest('.readNotice');
            var viewed_tagObj = $(this).parents('.readNotice').find('input[type=radio]:checked');
            var viewed_tag = viewed_tagObj.val();
            var recObj = $(this).closest('.readNotice');
            var rec_id = $(recObj).attr('rec_id');
                //alert(viewed_tag);
            var parent = $(this).closest('.innerContent');
            /*if(viewed_tag == 1){
                $('.toggleContent', parent).hide();
            } else {
                $('.toggleContent', parent).show();
            }*/

            var url = '/index.php?module=webBasic_home&_spAction=updateNoticeParent&showHTML=0';
            $.get(url, {viewed_tag: viewed_tag, rec_id: rec_id}, function(json){
            });
        });

        $('.toggleIcon img').livequery('click', function(){
            var parent = $(this).closest('.innerContent');
            //$(this).attr('src','/www/themes/Kite/images/down.png');
            $('.toggleContent', parent).slideToggle();
        });

        $('.btnSubmit').livequery('click', function(){
            var parent = $(this).closest('.innerContent');
            var notice_id = $(this).attr('notice_id');
            var student_id = $(this).attr('student_id');
            var notes = $('#fld_notes').val();
            var url = '/index.php?module=webBasic_home&_spAction=displayFeedback&showHTML=0';
            $.get(url, {notice_id:notice_id, student_id: student_id, notes: notes},  function(html){
                var url = '/index.php?module=webBasic_home&_spAction=displayFeedback&showHTML=0';
                $.get(url, {notice_id:notice_id, student_id: student_id}, function(html){
                    $('#fld_notes', parent).val('');
                    $('.feedbackDisplay', parent).html(html);
                    Util.hideProgressInd();
                });
            });
        });

        $("select[name='student_id']").change(function() {
            var student_id = $("select[name='student_id']").val();
            var url = '/index.php?module=edukiteWeb_notice&_spAction=studentIdForParent&showHTML=0';
            $.get(url, {student_id: student_id}, function (data) {
                window.location.reload(true);
            });
        });

        $('.summaryLink').livequery('click', function(){
            window.location.reload(true);
        });

        $('.dailyNotice a').livequery('click', function(){
            window.location.reload(true);
        });

       /* $('.calendarDisplay').livequery(function(){
            $(this).each(function(){
                var calId = $(this).attr('id');

                exp = {
                     handle: calId
                    ,eventAction: '/index.php?widget=edukite_calendarDisplay&_spAction=eventDetails&showHTML=0'
                    ,headerObj: {
                         left: 'prev,next'
                        ,center: 'title'
                        ,right: ''
                    }
                    ,timeFormatObj: {month: '','': ''}
                    ,minTime: 8
                }
                cpw.edukite.calendarDisplay.run(exp);
            });
        });*/

        $('.dailyCalendar a').livequery('click', function(){
            var student_id = $(this).attr('student_id');
            var status = $(this).attr('status');
            var archive = $(this).attr('archive');
            $('.dailyNotice a').removeClass('active');
            $('.dailyCalendar a').addClass('active');
            //$('.dailyCalendarShowHide').hide();

            var url = '/index.php?module=edukiteWeb_notice&_spAction=activityCalendarDisplay&showHTML=0';
            $.get(url, {student_id: student_id, status: status, archive: archive}, function(html){
                $('.homeLeft').html(html);
                    $('.homeLeft .inner .jqGalleriaSlider').each(function(){
                        var galId = $(this).attr('id');

                        exp = {
                             handle: galId
                            ,width: '287'
                            ,height: '200'
                            ,autoplay: ''
                            ,speed: '5'
                            ,zoom: ''
                            ,showCaption: ''
                            ,thumbnails: ''
                        }
                        cpw.media.relatedImages.run(exp);
                    });
                Util.hideProgressInd();
            });
        });

        $('.popUpContent .jqGalleriaSlider').livequery(function(){
            $(this).each(function(){
                var galId = $(this).attr('id');

                exp = {
                     handle: galId
                    ,width: '600'
                    ,height: '400'
                    ,autoplay: ''
                    ,speed: '5'
                    ,zoom: ''
                    ,showCaption: ''
                    ,thumbnails: ''
                }
                cpw.media.relatedImages.run(exp);
            });
        });
    },

    reloadParentFeedback: function(){
        var url = '/index.php?module=webBasic_home&_spAction=displayFeedback&showHTML=0';
        $.get(url,  function(html){
            $('#fld_notes', parent).val('');
            $('.feedbackDisplay', parent).html(html);
            Util.hideProgressInd();
        });
    }

}