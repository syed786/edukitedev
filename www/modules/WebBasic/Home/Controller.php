<?
class CPL_Www_Modules_WebBasic_Home_Controller extends CP_Www_Modules_WebBasic_Home_Controller
{
    function __construct() {
        $tv = Zend_Registry::get('tv');
        $cpCfg = Zend_Registry::get('cpCfg');

        $tv['pageCSSClassTop'] = "home";
        CP_Common_Lib_Registry::arrayMerge('tv', $tv);
    }

    /**
     *
     */
    function getDailyDairy() {
        $modObj = getCPModuleObj('edukiteWeb_notice');
        return $modObj->view->getDailyDairy();
    }

    /**
     *
     */
    function getUpdateNoticeParent() {
        $modObj = getCPModuleObj('edukiteWeb_notice');
        return $modObj->view->getUpdateNoticeParent();
    }

    /**
     *
     */
    function getDisplayFeedback() {
        $modObj = getCPModuleObj('edukiteWeb_notice');
        return $modObj->view->getDisplayFeedback();
    }

    /**
     *
     */
    function getAddFeedbackSubmit() {
        $modObj = getCPModuleObj('edukiteWeb_notice');
        return $modObj->model->getAddFeedbackSubmit();
    }

    /**
     *
     */
    function getPrintGalleryAsPdf() {
         return $this->view->getPrintGalleryAsPdf();
    }
}