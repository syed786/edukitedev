<?
class CPL_Www_Modules_WebBasic_Home_View extends CP_Www_Modules_WebBasic_Home_View
{
    /*
     *
     */
    function getList($dataArray) {
        $modObj = getCPModuleObj('edukiteWeb_notice');
        return $modObj->view->getList();
    }

    /**
     *TCPDF FORMAT
     */
    function getPrintGalleryAsPdf() {
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $searchVar = Zend_Registry::get('searchVar');
        $media = Zend_Registry::get('media');
        $cpPaths = Zend_Registry::get('cpPaths');
        $dbUtil = Zend_Registry::get('dbUtil');
        $dateUtil = Zend_Registry::get('dateUtil');


        ini_set('memory_limit', '512M');

        set_time_limit(50000);

       // include_once(CP_LIBRARY_PATH.'lib_php/tcpdf/tcpdf.php');
        //include_once(CP_LIBRARY_PATH.'lib_php/tcpdf-extra/tcpdf.php');
       include_once(CP_CORE_PATH.'CP/www/modules/WebBasic/Home/headfoot.php');

        //$pdf = new MYPDF2();
        // create new PDF document
        $pdf = new MYPDF_Local(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edukite Gallery');
        $pdf->SetTitle('Edukite Gallery');
        $pdf->SetSubject('Edukite Gallery');
        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 04', PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER, 10);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER, 0);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // convert TTF font to TCPDF format and store it on the fonts folder
        $fontname = TCPDF_FONTS::addTTFfont(CP_LIBRARY_PATH.'/fonts/Arial/arial.ttf', 'TrueTypeUnicode', '', 96);

        // ---------------------------------------------------------QUOTE QUERY START

        //$pdf->SetFont('Arial','B',10);
        $pdf->AddPage();
        $pdf->SetFont($fontname, 'B', 11, '', false);

        $contact_id    = $fn->getReqParam('contact_id');
        $status        = $fn->getReqParam('status');
        $notice_id     = $fn->getReqParam('notice_id');
        $currentDate   = date('Y-m-d');
        $teacherKiteId = $fn->getReqParam('teacherKiteId'); 

        if($status != ''){
            $status = "AND n.status = '{$status}'";
        }

        $kite_id = "AND ns.student_id = {$contact_id}";
        
        $SQL = "
        SELECT n.* ,CONCAT_WS(' ', t.first_name, t.last_name) AS teacher_name
        FROM notice n 
        LEFT JOIN (teacher t) ON (t.teacher_id = n.teacher_id) 
        LEFT JOIN (notice_student ns) ON (ns.notice_id = n.notice_id) 
        WHERE n.launch_now = 1 
        {$kite_id}
        AND (n.expiry_date >= '{$currentDate}' 
            OR n.expiry_date = '' 
            OR n.expiry_date IS NULL) 
        AND n.notice_id = {$notice_id}
        {$status}
        GROUP BY ns.notice_id 
        ORDER BY n.launch_date DESC, n.notice_id DESC 
        ";
        $result         = $db->sql_query($SQL);
        $numRows        = $db->sql_numrows($result);
        $row            = $db->sql_fetchrow($result);
        $today = date("Y-m-d");
        $count = 0;

        $SQL = "
        SELECT *
        FROM media
        WHERE record_id = '{$contact_id}'
        AND room_name = 'edukite_student'
        AND record_type =  'picture'
        ";

        $resultStudentImage  = $db->sql_query($SQL);
        $rowStudentImage     = $db->sql_fetchrow($resultStudentImage);
        $studentRec    = $fn->getRecordRowByID('student', 'student_id', $contact_id);

        $studentImage = '<img border="0" src="/media/large/'.$rowStudentImage['file_name'].'" width="60"/>';        

        $tb1Title ='
        <table border="0" cellpadding="4" width="100%">
          <tr>
             <td width="85%" color="#447EB5" align="centre" style="font-size:24px; font-weight:bold;"><br/><br/>'.$row['title'].'</td>   
             <td width="15%" style="font-weight:bold;" align ="centre">'.$studentImage.'<br/>'.$studentRec['first_name'].' '.$studentRec['last_name'].'</td>
          </tr>
        </table>
        ';

        $pdf->writeHTML($tb1Title, true, false, false, false, '');
        
        $tb1Gallery ='<table border="0" cellpadding="4" width="100%">';
        $Image = '';
        //while ($row = $db->sql_fetchrow($result)) {
            $launch_date = $fn->getCPDate($row['launch_date'], 'D d F Y');
            $tb1Gallery = $tb1Gallery.'
            <tr bgcolor="#A8C6FE" style="color: #FE4114;font-weight:bold;">
                <td width="9%">TITLE:</td>   
                <td width="36%">'.$row['title'].'</td>   
                <td width="5%">by</td>   
                <td width="25%">'.$row['teacher_name'].'</td>   
                <td width="25%">'.$launch_date.'</td>   
            </tr>
            <tr>
                <td width="100%">'.$row['description'].'</td>
            </tr>
            ';

            $SQL = "
            SELECT *
            FROM media m
            LEFT JOIN (notice n) ON (n.notice_id = m.record_id)
            WHERE m.record_id = {$row['notice_id']}
            AND record_type = 'picture'
            ORDER BY COALESCE(sort_order, 999999999) ASC
            ";

            $resultImage     = $db->sql_query($SQL);
            $tb1Gallery = $tb1Gallery.'<tr nobr="true">';
            $i = 0;
            while ($rowImage = $db->sql_fetchrow($resultImage)) {
               if($i == 2){
                    $tb1Gallery = $tb1Gallery.'</tr><tr nobr="true">';
                    $i = 1;
               }else{
                    $i++;                        
               }

               $tb1Gallery = $tb1Gallery.' 
                <td width="50%" align="left">
                    <img src="/media/large/'.$rowImage['file_name'].'" width="298" height="187" />
                </td>
                ';
            }  
            $tb1Gallery = $tb1Gallery.'</tr>';                  

        ///$count++;
        //}

        $tb1Gallery = $tb1Gallery.'</table>';

        if($numRows == 0){
            $tb1noResult ='<div style = "font-size:18px; font-weight:bold;">No Gallery record found for this student.
            </div>';
             $pdf->writeHTML($tb1noResult, true, false, false, false, '');
        }else{
            $pdf->writeHTML($tb1Gallery, true, false, false, false, '');
        }

        $pdf->Output('edukite_gallery.pdf', 'I');

    }
}
