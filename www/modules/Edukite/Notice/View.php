<?
class CPL_Www_Modules_Edukite_Notice_View extends CP_Www_Modules_Edukite_Notice_View
{

     /**
     *
     */

    function getNoticeOptions(){
        $cpUrl  = Zend_Registry::get('cpUrl');

        $kitePostUrl   = "new/?template=kitePost";
        $dailyDiaryUrl = "new/?template=dailyDiary";
        $galleryUrl    = "new/?template=gallery";

        $text = "
        <div class='noticeType'>
            <div class='dailyDiary noticeTypeBg'><a href='{$dailyDiaryUrl}'><span>Daily Diary</span></a></div>
            <div class='kitePost noticeTypeBg'><a href='{$kitePostUrl}'><span>Learning journey</span></a></div>
            <div class='gallery noticeTypeBg'><a href='{$galleryUrl}'><span>Gallery</span></a></div>
        </div>
        ";

        return $text;
    }


}