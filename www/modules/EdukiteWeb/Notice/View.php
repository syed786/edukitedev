<?
class CPL_Www_Modules_EdukiteWeb_Notice_View extends CP_Www_Modules_EdukiteWeb_Notice_View
{
    /**
     *
     */
    function getDetail1($row){
        $media = Zend_Registry::get('media');
        $cpCfg = Zend_Registry::get('cpCfg');
        $ln = Zend_Registry::get('ln');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $cpUrl = Zend_Registry::get('cpUrl');
        $teacherKiteId = $fn->getSessionParam('teacherKiteId');

        $wImagesSlider = getCPWidgetObj('media_bsCarousel');
        $desc = nl2br($row['description']);

        $student_id = ($tv['sitePfxId'] != '') ? $tv['sitePfxId'] :  $_SESSION['cpContactId'];
        $urlArray = array();
        $urlArray['siteType'] = 'kite';
        $secRec = getCPModelObj('webBasic_section')->getRecordByType('Home');
        $urlArray['section_title'] = $secRec['title'];
        $urlArray['sitePfxId'] = $student_id;
        $kiteUrl = $cpUrl->make_seo_url($urlArray);

        $links = "";
        $website = "";
        $youtube = "";
        $addFeedback = '';

        if($teacherKiteId == 1){
            $returnUrl = $kiteUrl . '?teacherKite=1';
        } else {
            $returnUrl = ($tv['sitePfxId'] != '') ? $kiteUrl :  '/kite/home/';
        }

        $attArr = $media->getFirstMediaRecord('edukite_notice', 'attachment', $row['notice_id']);

        if (count($attArr) > 0){
            $links = "
            <div class='links'>
                {$media->getMediaFilesDisplayThin('edukite_notice', 'attachment', $row['notice_id'])}
            </div>
            ";
        }

        if($row['links'] != ''){
            $website_link = $row['links'];
            $seplinks = '';
            $linkarray   = explode("\n", $row['links']);
            foreach ($linkarray as &$alink) {
                $webHttp = substr($alink, 0, 4);
                if($webHttp == 'http'){
                    $alink = $alink;
                } else {
                    $alink = 'http://' . $alink;
                }
                $seplinks .= "
                    <a href='{$alink}' target='_blank'>{$alink}</a><br>
                ";
            }
                $website = "
                <div class='links'>
                {$seplinks}
                </div>
                ";
        }

        if($row['youtube_links'] != ''){
            $ytarray     =explode("/", $row['youtube_links']);
            $ytendstring =end($ytarray);
            $ytendarray  =explode("?v=", $ytendstring);
            $ytendstring =end($ytendarray);
            $ytendarray  =explode("&", $ytendstring);
            $ytcode      =$ytendarray[0];
            $youtube ="
            <div class='youTube'>
                <iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/$ytcode?rel=0\" frameborder=\"0\" allowfullscreen></iframe>
            </div>
            ";
        }

        $activeTab = '';
        if($cpCfg['cp.primarySchool'] == 1){
            if($row['template'] == 'Daily Diary'){
                $activeTab = "<div class='calendarBannerActive'></div>";
            } else if ($row['template'] == 'Kite Post'){
                $activeTab = "<div class='noticeBannerActive'></div>";
            } else if ($row['template'] == 'Gallery'){
                $activeTab = "<div class='lockerBannerActive'></div>";
            }
        } else {
            if($row['template'] == 'Daily Diary'){
                $activeTab = "<div class='dailyDairyBannerActive'></div>";
            } else if ($row['template'] == 'Kite Post'){
                $activeTab = "<div class='kitePostBannerActive'></div>";
            } else if ($row['template'] == 'Gallery'){
                $activeTab = "<div class='galleryBannerActive'></div>";
            }
        }

        if(($_SESSION['cpLoginTypeWWW'] == 'edukite_parent' && $row['parent_feedback'] == 1) || ($_SESSION['cpLoginTypeWWW'] == 'edukite_teacher' && $row['parent_feedback'] == 1)){
            $histRec    = $fn->getRecordRowByID('student_parent', 'student_id', $student_id);

            $feedbackTitle = '';
            if($histRec['parent_id'] != ''){
                $commentChk = $this->getDisplayFeedback($row['notice_id']);
                if($commentChk != '' || $row['teacher_id'] == $_SESSION['cpContactId'] || $_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                    if($_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                        $feedbackTitle = 'Discussion Board';
                    } else if($_SESSION['cpLoginTypeWWW'] == 'edukite_teacher'){
                        $feedbackTitle = 'Discussion Board';
                    }
                }

                $feedbackBox = '';
                if($row['teacher_id'] == $_SESSION['cpContactId'] || $_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                    $feedbackBox ="{$this->getAddFeedback($row['notice_id'], $student_id)}";
                }
                $addFeedback ="
                <div class='feedbackTitle'>{$feedbackTitle}</div>
                <div class='feedbackDisplay'>
                    {$this->getDisplayFeedback($row['notice_id'])}
                </div>
                {$feedbackBox}
                ";
            }
        }
        $acheivementRow  = '';

        if ($cpCfg['showAcheivement'] == 1 && ($row['template']  == 'Kite Post' || $row['template'] == 'Daily Diary')){
                $acheivementRow = $this->getAchievementDisplay($row['notice_id'], $student_id);
        }

        $mediaRec = $fn->getRecordByCondition('media',
                                                    "record_id = {$row['notice_id']} AND
                                                     media_type = 'image' AND
                                                     record_type = 'picture'
                                                    ");

        if($mediaRec){
            $pic = "
            {$wImagesSlider->getWidget(array(
                 'module'    => 'edukite_notice'
                ,'record_id' => $row['notice_id']
                ,'useRecType1Only' => true
            ))}
            ";
        } else {
            $pic ="
            <h1>{$row['title']}</h1>
            <p>{$desc}</p>
            ";
        }

        $text = "
        {$activeTab}
        <div class='returnHome'>
            <a href='{$returnUrl}' class='backToList txtCenter'>return</a>
        </div>
        <div class='header'>
        </div>
        <div class='productDetail'>
            {$pic}
            {$acheivementRow}
            {$links}
			{$website}
            {$youtube}
            {$addFeedback}
        </div>
        ";

        return $text;
    }

    /**
     *
     */
    function getListRow1($template, $teacherKite) {
        $media = Zend_Registry::get('media');
        $cpCfg = Zend_Registry::get('cpCfg');
        $cpUrl = Zend_Registry::get('cpUrl');
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $tv = Zend_Registry::get('tv');
        $formObj = Zend_Registry::get('formObj');

        $rows = '';
        $activityDate = '';
        $subjectId = '';
        $noticeType = '';
        $edit='';
        $readNotice = '';
        $class='';
        $addFeedback = '';
        $expiryDate = '';
        $teacherRole = '';

        $student_id = ($tv['sitePfxId'] != '') ? $tv['sitePfxId'] :  $_SESSION['cpContactId'];

        $currentYear = date("Y");
        $currentDate = date('Y-m-d');
        $notice_type_id = $fn->getReqParam('notice_type_id');
        $subject_id     = $fn->getReqParam('subject_id');
        $teacherKite     = $fn->getReqParam('teacherKite');

        // For Parent Login we need to get the student id , below code is for the same
        if($_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
            $histRec    = $fn->getRecordRowByID('student_parent', 'parent_id', $_SESSION['cpContactId']);
            if($_SESSION['student_id'] != ''){
                $studRec    = $fn->getRecordRowByID('student', 'student_id', $_SESSION['student_id'] );
            } else {
                $studRec    = $fn->getRecordRowByID('student', 'student_id', $histRec['student_id'] );
            }
            $student_id = $studRec['student_id'] ;
        }

        if($_SESSION['cpLoginTypeWWW'] == 'edukite_teacher'){
            $teacherRec    = $fn->getRecordRowByID('teacher', 'teacher_id', $_SESSION['cpContactId']);
            $teacherRole = $teacherRec['role'];
        }

        if($teacherKite == 1){
            $kite_id = "AND ns.teacher_id = {$student_id}";
        } else {
            $kite_id = "AND ns.student_id = {$student_id}";
        }

        //This sql used to find the latest activity_date to display all the records for that date in daily dairy
        $SQLDailyDairy = "
        SELECT n.*
        FROM notice n
        LEFT JOIN (notice_student ns) ON (ns.notice_id = n.notice_id)
        WHERE n.template = 'Daily Diary'
          AND n.status = 'Active'
          AND n.launch_now = 1
          {$kite_id}
          GROUP BY ns.notice_id
          ORDER BY n.notice_id DESC
        ";
        $resultDailyDairy  = $db->sql_query($SQLDailyDairy);
        $rowDailyDairy = $db->sql_fetchrow($resultDailyDairy);
        $activity_date = $rowDailyDairy['activity_date'];

        if($template == 'Daily Diary'){
            if($cpCfg['cp.primarySchool'] == 1){
                $activityDate = "AND n.activity_date >= '{$currentDate}'";
            } else {
                $activityDate = "AND n.activity_date = '{$activity_date}'";
            }
        }

        if($notice_type_id != '' && $template == 'Kite Post'){
            $notice_type_id = "AND n.notice_type_id = '{$notice_type_id}'";
        }
        if($subject_id != '' && $template == 'Kite Post'){
            $subjectId = "AND n.subject_id = {$subject_id}";
        }

        if($template == 'Kite Post' || $template == 'Gallery' || $template == 'Kite Post Left'){
            $expiryDate = "AND (n.expiry_date >= '{$currentDate}' OR n.expiry_date = '' OR n.expiry_date IS NULL)";
        }

        if($template == 'Gallery' || $template == 'Daily Diary'){
            $notice_type_id  ='';
        }

        if($cpCfg['cp.primarySchool'] == 1 && $template == 'Daily Diary'){
            $orderBy = "ORDER BY n.activity_date";
        } else if($template == 'Gallery'){
            $orderBy = "ORDER BY n.launch_date DESC, n.notice_id DESC";
        } else {
            $orderBy = "ORDER BY n.launch_date DESC, n.notice_id DESC";
        }

        //in the sql group by is used to eliminate duplicates if the same notice is linked through class and cohort and individual or either two of them.
        $SQL = "
        SELECT n.*
              ,CONCAT_WS(' ', t.first_name, t.last_name) AS teacher_name
        FROM notice n
        LEFT JOIN (teacher t) ON (t.teacher_id = n.teacher_id)
        LEFT JOIN (notice_student ns) ON (ns.notice_id = n.notice_id)
        WHERE n.template = '{$template}'
          AND n.status = 'Active'
          AND n.launch_now = 1
          {$activityDate}
          {$kite_id}
          {$subjectId}
          {$expiryDate}
          {$notice_type_id}
          GROUP BY ns.notice_id
          {$orderBy}
        ";
        $result  = $db->sql_query($SQL);

        while ($row = $db->sql_fetchrow($result)) {
            $exp = array('secType' => 'Kite Notice');
            $detailUrl = $cpUrl->getUrlByRecord($row, 'notice_id', $exp);
            //$detailUrl = "/index.php?module=edukiteWeb_notice&_spAction=detailPopUp&notice_id={$row['notice_id']}&showHTML=0";

            $instName = 'wImagesSlider' . $row['notice_id'];
            $$instName = getCPWidgetObj('media_imagesSlider');

            $website = '';
            $links = '';
            $youtube = '';
            if($template == 'Kite Post' || $template == 'Daily Diary' || $template == 'Kite Post Left'){
                if($template == 'Kite Post'){
                    $pic = "
                    {$$instName->getWidget(array(
                         'module'    => 'edukite_notice'
                        ,'record_id' => $row['notice_id']
                        ,'width'     => 380
                        ,'height'    => 320
                        ,'handle'    => 'slider' . $row['notice_id']
                        ,'zoom'      => false
                        ,'thumbnails' => false
                        ,'showCaption' => false
                        ,'useRecType1Only' => true
                    ))}
                    ";
                } else {
                    $pic = "
                    {$$instName->getWidget(array(
                         'module'    => 'edukite_notice'
                        ,'record_id' => $row['notice_id']
                        ,'width'     => 287
                        ,'height'    => 200
                        ,'handle'    => 'slider' . $row['notice_id']
                        ,'zoom'      => false
                        ,'thumbnails' => false
                        ,'showCaption' => false
                        ,'useRecType1Only' => true
                    ))}
                    ";
                }

                if($row['links'] != ''){
                    $website_link = $row['links'];
                    $seplinks = '';
                    $linkarray   = explode("\n", $row['links']);
                    foreach ($linkarray as &$alink) {
                        $webHttp = substr($alink, 0, 4);
                        if($webHttp == 'http'){
                            $alink = $alink;
                        } else {
                            $alink = 'http://' . $alink;
                        }
                        $seplinks .= "
                            <a href='{$alink}' target='_blank'>{$alink}</a><br>
                        ";
                    }
                        $website = "
                        <div class='links'>
                        {$seplinks}
                        </div>
                        ";
                }
                $attArr = $media->getFirstMediaRecord('edukite_notice', 'attachment', $row['notice_id']);
                if (count($attArr) > 0){
                    $links = "
                    <div class='links'>
                        {$media->getMediaFilesDisplayThin('edukite_notice', 'attachment', $row['notice_id'])}
                    </div>
                    ";
                }
                if($row['youtube_links'] != ''){
                    $ytarray     =explode("/", $row['youtube_links']);
                    $ytendstring =end($ytarray);
                    $ytendarray  =explode("?v=", $ytendstring);
                    $ytendstring =end($ytendarray);
                    $ytendarray  =explode("&", $ytendstring);
                    $ytcode      =$ytendarray[0];
                    $youtube ="
                    <div class='youTube'>
                        <iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/$ytcode?rel=0\" frameborder=\"0\" allowfullscreen></iframe>
                    </div>
                    ";
                }

                $readNotice = '';
                $addFeedback = '';
                $openExpanded = '';
                $acheivementRow = '';

                if($_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                    $noticeParent = $fn->getRecordByCondition('notice_parent',
                                                                "notice_id = {$row['notice_id']} AND
                                                                 student_id = {$student_id} AND
                                                                 parent_id = {$_SESSION['cpContactId']}
                                                                ");
                    $viewed_tag_fld = 'viewed_tag_' . $row['notice_id'];
                    $class='';
                    if($noticeParent['viewed_tag'] == 1){
                        $class='hideContent';
                        $arrow = "<img src='/www/themes/Kite/images/up.png'/ title=''>";
                    } else {
                        $arrow = "<img src='/www/themes/Kite/images/down.png'/ title=''>";
                    }
                    $readNotice = "
                    <div class='ym-contain-dt'>
                        <div class='readNotice float_left' rec_id='{$noticeParent['notice_parent_id']}'>
                            {$formObj->getYesNoRRow('I have read this notice:', $viewed_tag_fld , $noticeParent['viewed_tag'])}
                        </div>
                    </div>
                    ";

                    if($noticeParent['viewed_tag'] == 1){
                        $openExpanded = 0;
                    }
                    else{
                        $openExpanded = 1;
                    }
                }

                if(($_SESSION['cpLoginTypeWWW'] == 'edukite_parent' && $row['parent_feedback'] == 1) || ($_SESSION['cpLoginTypeWWW'] == 'edukite_teacher' && $row['parent_feedback'] == 1)){
                    $histRec    = $fn->getRecordRowByID('student_parent', 'student_id', $student_id);

                    $feedbackTitle = '';
                    if($histRec['parent_id'] != ''){
                        $commentChk = $this->getDisplayFeedback($row['notice_id']);
                        if($commentChk != '' || $row['teacher_id'] == $_SESSION['cpContactId'] || $_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                            if($_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                                $feedbackTitle = 'Discussion Board';
                            } else if($_SESSION['cpLoginTypeWWW'] == 'edukite_teacher'){
                                $feedbackTitle = 'Discussion Board';
                            }
                        }

                        $feedbackBox = '';
                        if($row['teacher_id'] == $_SESSION['cpContactId'] || $_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                            $feedbackBox ="{$this->getAddFeedback($row['notice_id'], $student_id)}";
                        }
                        $addFeedback ="
                        <div class='feedbackTitle'>{$feedbackTitle}</div>
                        <div class='feedbackDisplay'>
                            {$this->getDisplayFeedback($row['notice_id'], $student_id)}
                        </div>
                        {$feedbackBox}
                        ";
                    }
                }
            }

            $edit='';
            if($_SESSION['cpLoginTypeWWW'] == 'edukite_teacher'){
                $teacherRec    = $fn->getRecordRowByID('teacher', 'teacher_id', $_SESSION['cpContactId']);
                if($teacherRec['role'] == 'Kite Master'){
                    $edit="
                    <div class='ym-contain-dt'>
                    <div class='mb5 float_left'>
                        <a href='/controller/notice/edit/{$row['notice_id']}/'><u>Edit</u></a>
                    </div>
                    </div>
                    ";
                } else {
                    if($row['teacher_id'] == $_SESSION['cpContactId']){
                        $edit="
                        <div class='ym-contain-dt'>
                        <div class='mb5 float_left'>
                            <a href='/controller/notice/edit/{$row['notice_id']}/'><u>Edit</u></a>
                        </div>
                        </div>
                        ";
                    }
                }
            }

            if ($cpCfg['showAcheivement'] == 1 && $template == 'Kite Post' ){
                    $acheivementRow = $this->getAchievementDisplay($row['notice_id'], $student_id);
            }
            if($template == 'Gallery'){
                $exp = array('style' => '', 'folder' => 'normal', 'limit' => 1, 'showCaption' => 0);
                $pic = $media->getMediaPicture('edukite_notice', 'picture', $row['notice_id'], $exp);
                $rows .= "
                <div class='galleryContent'>
                    {$edit}
                    <div class='mt5'>
                        <a href='{$detailUrl}' class='' wrapperId='detail'>{$pic}</a>
                    </div>
                    <h4><a href='{$detailUrl}' class='' wrapperId='detail'>{$ln->gfv($row, 'title', '0')}</a></h4>
                </div>
                ";
            } else if($template == 'Kite Post' || ($template == 'Daily Diary' && $cpCfg['cp.primarySchool'] == 0)){
                $desc = nl2br($row['description']);

                if($template == 'Daily Diary'){
                    $date = $row['activity_date'];
                } else {
                    $date = $row['launch_date'];
                }

                if($_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                    $rows .= "
                    <div class='innerContent'>
                        <div class='mt10 mb10 ym-contain-dt'>
                            <div class='float_left date'><i>{$row['teacher_name']}</i></div>
                            <div class='float_right date'><i>{$fn->getCPDate($date, 'D d F Y')}</i></div>
                        </div>
                        <h1><a href='{$detailUrl}' class='' wrapperId='detail'>{$ln->gfv($row, 'title', '0')}</a></h1>
                        <div class='linkPortalWrapper'>
                            <div expanded='{$openExpanded}' class='header'>
                                <div class='toggle minus'>&nbsp;</div>
                            </div>
                            <div class='mediaFilesDisplayWrap'>
                            <div class='toggleContent'>
                                <div class='description'>
                                    <p>{$desc}</p>
                                </div>
                                {$pic}
                                {$links}
                                {$website}
                                {$youtube}
                                {$addFeedback}
                                {$acheivementRow}
                            </div>
                            </div>
                        </div>
                        {$readNotice}
                    </div>
                    ";
                } else {
                    $rows .= "
                    <div class='innerContent'>
                        <div class='mt10 mb10 ym-contain-dt'>
                            <div class='float_left date'><i>{$row['teacher_name']}</i></div>
                            <div class='float_right date'><i>{$fn->getCPDate($date, 'D d F Y')}</i></div>
                        </div>
                        {$edit}
                        <h1><a href='{$detailUrl}' class='' wrapperId='detail'>{$ln->gfv($row, 'title', '0')}</a></h1>
                        <div class='description'>
                            <p>{$desc}</p>
                        </div>
                        {$pic}
                        {$links}
                        {$website}
                        {$youtube}
                        {$addFeedback}
                        {$acheivementRow}
                   </div>
                    ";
                }
            } else {
                $desc = nl2br($row['description']);
                $date = $row['activity_date'];
                $rows .= "
                <div class='innerContent'>
                    <div class='mt10 mb10 ym-contain-dt'>
                        <div class='float_left date'><i>{$row['teacher_name']}</i></div>
                        <div class='float_right date'><i>{$fn->getCPDate($date, 'D d F Y')}</i></div>
                    </div>
                    {$edit}
                    <h1><a href='{$detailUrl}' class='' wrapperId='detail'>{$ln->gfv($row, 'title', '0')}</a></h1>
                    <div class='description'>
                        <p>{$desc}</p>
                    </div>
                    {$pic}
                    {$links}
                    {$website}
                    {$youtube}
                    {$addFeedback}
                </div>
                ";
            }
        }

        $text = "
        <div class='inner'>
            {$rows}

        </div>
        ";
        return $text;
    }

    /**
     *
     */
    function getDetail2($row){
        $media = Zend_Registry::get('media');
        $cpCfg = Zend_Registry::get('cpCfg');
        $ln = Zend_Registry::get('ln');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $cpUrl = Zend_Registry::get('cpUrl');
        $teacherKiteId = $fn->getSessionParam('teacherKiteId');

        //$wImagesSlider = getCPWidgetObj('media_imagesSlider');
        $wImagesSlider = getCPWidgetObj('media_bsCarousel');
        $desc = nl2br($row['description']);

        $student_id = ($tv['sitePfxId'] != '') ? $tv['sitePfxId'] :  $_SESSION['cpContactId'];
        $urlArray = array();
        $urlArray['siteType'] = 'kite';
        $secRec = getCPModelObj('webBasic_section')->getRecordByType('Home');
        $urlArray['section_title'] = $secRec['title'];
        $urlArray['sitePfxId'] = $student_id;
        $kiteUrl = $cpUrl->make_seo_url($urlArray);

        $links = "";
        $website = "";
        $youtube = "";
        $addFeedback = '';

        if($teacherKiteId == 1){
            $returnUrl = $kiteUrl . '?teacherKite=1';
        } else {
            $returnUrl = ($tv['sitePfxId'] != '') ? $kiteUrl :  '/kite/home/';
        }

        $attArr = $media->getFirstMediaRecord('edukite_notice', 'attachment', $row['notice_id']);

        if (count($attArr) > 0){
            $links = "
            <div class='links'>
                {$media->getMediaFilesDisplayThin('edukite_notice', 'attachment', $row['notice_id'])}
            </div>
            ";
        }

        if($row['links'] != ''){
            $website_link = $row['links'];
            $seplinks = '';
            $linkarray   = explode("\n", $row['links']);
            foreach ($linkarray as &$alink) {
                $webHttp = substr($alink, 0, 4);
                if($webHttp == 'http'){
                    $alink = $alink;
                } else {
                    $alink = 'http://' . $alink;
                }
                $seplinks .= "
                    <a href='{$alink}' target='_blank'>{$alink}</a><br>
                ";
            }
                $website = "
                <div class='links'>
                {$seplinks}
                </div>
                ";
        }

        if($row['youtube_links'] != ''){
            $ytarray     =explode("/", $row['youtube_links']);
            $ytendstring =end($ytarray);
            $ytendarray  =explode("?v=", $ytendstring);
            $ytendstring =end($ytendarray);
            $ytendarray  =explode("&", $ytendstring);
            $ytcode      =$ytendarray[0];
            $youtube ="
            <div class='youTube'>
                <iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/$ytcode?rel=0\" frameborder=\"0\" allowfullscreen></iframe>
            </div>
            ";
        }

        $activeTab = '';
        if($cpCfg['cp.primarySchool'] == 1){
            if($row['template'] == 'Daily Diary'){
                $activeTab = "<div class='calendarBannerActive'></div>";
            } else if ($row['template'] == 'Kite Post'){
                $activeTab = "<div class='noticeBannerActive'></div>";
            } else if ($row['template'] == 'Gallery'){
                $activeTab = "<div class='lockerBannerActive'></div>";
            }
        } else {
            if($row['template'] == 'Daily Diary'){
                $activeTab = "<div class='dailyDairyBannerActive'></div>";
            } else if ($row['template'] == 'Kite Post'){
                $activeTab = "<div class='kitePostBannerActive'></div>";
            } else if ($row['template'] == 'Gallery'){
                $activeTab = "<div class='galleryBannerActive'></div>";
            }
        }

        if(($_SESSION['cpLoginTypeWWW'] == 'edukite_parent' && $row['parent_feedback'] == 1) || ($_SESSION['cpLoginTypeWWW'] == 'edukite_teacher' && $row['parent_feedback'] == 1)){
            $histRec    = $fn->getRecordRowByID('student_parent', 'student_id', $student_id);

            $feedbackTitle = '';
            if($histRec['parent_id'] != ''){
                $commentChk = $this->getDisplayFeedback($row['notice_id']);
                if($commentChk != '' || $row['teacher_id'] == $_SESSION['cpContactId'] || $_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                    if($_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                        $feedbackTitle = 'Discussion Board';
                    } else if($_SESSION['cpLoginTypeWWW'] == 'edukite_teacher'){
                        $feedbackTitle = 'Discussion Board';
                    }
                }

                $feedbackBox = '';
                if($row['teacher_id'] == $_SESSION['cpContactId'] || $_SESSION['cpLoginTypeWWW'] == 'edukite_parent'){
                    $feedbackBox ="{$this->getAddFeedback($row['notice_id'], $student_id)}";
                }
                $addFeedback ="
                <div class='feedbackTitle'>{$feedbackTitle}</div>
                <div class='feedbackDisplay'>
                    {$this->getDisplayFeedback($row['notice_id'])}
                </div>
                {$feedbackBox}
                ";
            }
        }
        $acheivementRow  = '';

        if ($cpCfg['showAcheivement'] == 1 && ($row['template']  == 'Kite Post' || $row['template'] == 'Daily Diary')){
                $acheivementRow = $this->getAchievementDisplay($row['notice_id'], $student_id);
        }

        $mediaRec = $fn->getRecordByCondition('media',
                                                    "record_id = {$row['notice_id']} AND
                                                     media_type = 'image' AND
                                                     record_type = 'picture'
                                                    ");

        if($mediaRec){
            $pic = "
            {$wImagesSlider->getWidget(array(
                 'module'    => 'edukite_notice'
                ,'record_id' => $row['notice_id']
                ,'useRecType1Only' => true
            ))}
            ";
        } else {
            $pic ="
            <h1>{$row['title']}</h1>
            <p>{$desc}</p>
            ";
        }

        /*$text = "
        {$activeTab}
        <div class='returnHome'>
            <a href='{$returnUrl}' class='backToList txtCenter'>return</a>
        </div>
        <div class='header'>
        </div>
        <div class='productDetail'>
            {$wImagesSlider->getWidget(array(
                 'module'    => 'edukite_notice'
                ,'record_id' => $row['notice_id']
                ,'width'     => 380
                ,'height'     => 320
                ,'zoom'      => true
                ,'thumbnails' => false
                ,'showCaption' => false
                ,'useRecType1Only' => true
            ))}
            <h1>{$ln->gfv($row, 'title')}</h1>
            <p>{$desc}</p>
            {$acheivementRow}
            {$links}
            {$website}
            {$youtube}
            {$addFeedback}
        </div>
        ";*/

        $text = "
        {$activeTab}
        <div class='returnHome'>
            <a href='{$returnUrl}' class='backToList txtCenter'>return</a>
        </div>
        <div class='header'>
        </div>
        <div class='productDetail'>
            {$pic}
            {$acheivementRow}
            {$links}
            {$website}
            {$youtube}
            {$addFeedback}
        </div>
        {$media->getRightPanelMediaDisplay('Picture', 'edukite_notice', 'picture', $row)}
        ";

        return $text;
    }
}