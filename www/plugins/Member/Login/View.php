<?
class CPL_Www_Plugins_Member_Login_View extends CP_Www_Plugins_Member_Login_View
{

    /**
     *
     */
    function getLoginLink() {
        $cpCfg = Zend_Registry::get('cpCfg');

        $ln = Zend_Registry::get('ln');
        $cpUrl = Zend_Registry::get('cpUrl');

        $loginUrl = $cpUrl->getUrlBySecType('Login');
        $text = "
        <!--<a class='btnLogin' href='{$loginUrl}'>
            <span>{$ln->gd('w.member.loginForm.form.lbl.login')}</span>
        </a>-->
        ";
        return $text;
    }

    /**
     *
     */
    function getLogoutLink() {
        $ln = Zend_Registry::get('ln');
        $cpCfg = Zend_Registry::get('cpCfg');

        $logoutUrl = '/index.php?plugin=member_login&_spAction=logout';

        $text = "
        <a class='btnLogout' href='{$logoutUrl}'>
            <span>{$ln->gd('logout')}</span>
        </a>
        ";
        return $text;
    }
}