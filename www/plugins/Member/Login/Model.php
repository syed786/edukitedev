<?
class CPL_Www_Plugins_Member_Login_Model extends CP_Www_Plugins_Member_Login_Model
{
    /**
     *
     */
    function getLoginSubmit() {
        $db = Zend_Registry::get('db');
        $ln = Zend_Registry::get('ln');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $cpCfg = Zend_Registry::get('cpCfg');
        $validate = Zend_Registry::get('validate');

        $email = $fn->getPostParam('email', '', true);
        $username = $fn->getPostParam('username', '', true);
        $pass_word = $fn->getPostParam('pass_word', '', true);
        $saveLogin = $fn->getPostParam('saveLogin', '', true);

        $loginType = $fn->getPostParam('loginType');
        $modDetail = $this->getModDetailsArray($loginType);
        //-------------------------------------------------------------//
        $valArr = $this->getLoginSubmitValidate();
        $hasError = $valArr[0];
        $xmlText = $valArr[1];

        if ($hasError) {
            return $xmlText;
        }

        $tableName = $modDetail['tableName'];
        $keyField = $modDetail['keyField'];
        $year = date('Y');
        if($loginType == 'edukite_student'){
            $SQL = "
            SELECT *
            FROM {$tableName}
            WHERE username = '{$username}'
              AND pass_word = '{$pass_word}'
            ";
        } else if($loginType == 'edukite_teacher'){
            $SQL = "
            SELECT *
            FROM {$tableName}
            WHERE email = '{$email}'
              AND pass_word = '{$pass_word}'
              AND status = 'Active'
            ";
        } else {
            $SQL = "
            SELECT *
            FROM {$tableName}
            WHERE email = '{$email}'
              AND pass_word = '{$pass_word}'
            ";
        }

        $result = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);

        if ($numRows > 0) {
            $row = $db->sql_fetchrow($result);

            if ($cpCfg['p.member.login.updateLoginCount']) {
                $this->updateLoginCount($tableName, $keyField, $row[$keyField]);
            }
            if ($cpCfg['p.member.login.updateLastLoggedIn']) {
                $this->updateLastLoggedIn($tableName, $keyField, $row[$keyField]);
            }

            $retUrl = $this->setSessionValuesAfterLogin($row, $saveLogin, $loginType);
            return $validate->getSuccessMessageXML($retUrl);
        }
    }

    /**
     *
     */
    function getLoginSubmitValidate() {
        $ln = Zend_Registry::get('ln');
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $cpCfg = Zend_Registry::get('cpCfg');
        $tv = Zend_Registry::get('tv');
        $db = Zend_Registry::get('db');

        if($cpCfg['cp.hasLoginHistory']){
            $this->resetStatusInLoginHistory();
        }

        $validate->resetErrorArray();
        $email = $fn->getPostParam('email', '', true);
        $username  = $fn->getPostParam('username', '', true);
        $pass_word = $fn->getPostParam('pass_word', '', true);
        $pass_word = $fn->getPostParam('pass_word', '', true);
        $loginType = $fn->getPostParam('loginType');
        $modDetail = $this->getModDetailsArray($loginType);

        $tableName = $modDetail['tableName'];
        $keyField = $modDetail['keyField'];
        $year = date('Y');
        if($loginType == 'edukite_student'){
            $SQL = "
            SELECT *
            FROM {$tableName}
            WHERE username = '{$username}'
              AND pass_word = '{$pass_word}'
            ";
        } else {
            $SQL = "
            SELECT *
            FROM {$tableName}
            WHERE email = '{$email}'
              AND pass_word = '{$pass_word}'
            ";
        }

        $result = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);

        if ($numRows == 0) {
            if($loginType == 'edukite_student'){
                $validate->errorArray['username']['name'] = "username";
                $validate->errorArray['username']['msg'] = $ln->gd("p.member.login.form.yearError");
            } else {
                $validate->errorArray['email']['name'] = "email";
                $validate->errorArray['email']['msg'] = $ln->gd("p.member.login.form.yearError");
            }
        }

        $text = "";
        if($loginType == 'edukite_student'){
            $isEmailInvalidFormat = $validate->validateData("username", $ln->gd("cp.form.fld.username.err"));
        } else {
            $isEmailInvalidFormat = $validate->validateData("email", $ln->gd("cp.form.fld.email.err"), "email", "", "3", "50");
        }
        $isPasswordInvalidFormat = $validate->validateData("pass_word", $ln->gd("cp.form.fld.password.err"), "empty", "", "6", "20");

        if($loginType != 'edukite_student'){
            if (!$isEmailInvalidFormat && !$isPasswordInvalidFormat) {
                $email     = $fn->getPostParam('email', '', true);
                $pass_word = $fn->getPostParam('pass_word', '', true);

                $row = $this->checkLogin($email, $pass_word);
                if (!$row) {
                    $validate->errorArray['email']['name'] = "email";
                    $validate->errorArray['email']['msg'] = $ln->gd("p.member.login.form.err.invalidLogin");
                    $validate->errorArray['pass_word']['name'] = "pass_word";
                    $validate->errorArray['pass_word']['msg'] = "";
                }
            }
        }
        else{
            $row = $this->checkLogin($username, $pass_word);
            if (!$row) {
                $validate->errorArray['username']['name'] = "username";
                $validate->errorArray['username']['msg'] = $ln->gd("p.member.login.form.err.invalidLogin");
                $validate->errorArray['pass_word']['name'] = "pass_word";
                $validate->errorArray['pass_word']['msg'] = "";
            }
        }

        if (count($validate->errorArray) == 0) {
            return array(0, $validate->getSuccessMessageXML());
        } else {
            $fn->resetCookie("cpWWWUserNameC");
            $fn->resetCookie("cpWWWPasswordC");
            return array(1, $validate->getErrorMessageXML());
        }

        return $text;
    }

    /**
     *
     */
    function setReturnUrlAfterLogin($row = '') {
        $cpCfg = Zend_Registry::get('cpCfg');
        $cpUrl = Zend_Registry::get('cpUrl');
        $fn = Zend_Registry::get('fn');
        $loginType = $fn->getPostParam('loginType');
        $tv = Zend_Registry::get('tv');

        $retUrlForm = $fn->getPostParam('returnUrl');

        if ($retUrlForm != ''){
            $retUrl = $retUrlForm;
        } else if (@$_SESSION['cpReturnUrlAfterLogin'] != ''){
            $retUrl = $_SESSION['cpReturnUrlAfterLogin'];
        } else if ($loginType == 'edukite_student' || $loginType == 'edukite_parent' ){
            $tv['siteType'] = 'kite';
            CP_Common_Lib_Registry::arrayMerge('tv', $tv);
            $retUrl = $cpUrl->getUrlBySecType('Home');
        } else if ($loginType == 'edukite_teacher'){
            $tv['siteType'] = 'controller';
            //$tv['sitePfxId'] = $_SESSION['cpContactId'];
            CP_Common_Lib_Registry::arrayMerge('tv', $tv);
            //$retUrl = $cpUrl->getUrlBySecType('Home').'?teacherKite=1';
            //$retUrl = $cpUrl->getUrlBySecType('Student');
            $retUrl = "/kite/{$_SESSION['cpContactId']}/home/?teacherKite=1";
        } else {
            $retUrl = '/';
        }

        if (isset($_SESSION['cpReturnUrlAfterLogin'])){
            unset($_SESSION['cpReturnUrlAfterLogin']);
        }

        return $retUrl;
    }

    /**
     *
     * @param type $email
     * @param type $pass_word
     * @return type
     */
    private function checkLogin($email, $pass_word) {
        $db = Zend_Registry::get('db');
        $cpCfg  = Zend_Registry::get('cpCfg');
        $cpUtil = Zend_Registry::get('cpUtil');
        $fn = Zend_Registry::get('fn');

        $loginType = $fn->getPostParam('loginType');
        $modDetail = $this->getModDetailsArray($loginType);
        $year = date('Y');

        $loginSuccess = false;
        $contactRow = null;

        if($loginType != 'edukite_student'){
            $SQL = "
            SELECT *
            FROM {$modDetail['tableName']}
            WHERE email = '{$email}'
              AND pass_word = '{$pass_word}'
            ";
            }
        else{
            $SQL = "
            SELECT *
            FROM {$modDetail['tableName']}
            WHERE username = '{$email}'
              AND pass_word = '{$pass_word}'
            ";
        }

        $result  = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);
        if ($numRows > 0) {
            $loginSuccess = true;
            $contactRow = $db->sql_fetchrow($result);
        }

        return $contactRow;
    }

    /**
     *
     * @return <type>
     */
    function setSessionValuesAfterLogin($row, $saveLogin, $loginType = '') {
        $fn = Zend_Registry::get('fn');
        $tv = Zend_Registry::get('tv');
        $cpCfg = Zend_Registry::get('cpCfg');

        if ($loginType == ''){
            $loginType = $fn->getPostParam('loginType', 'membership_contact');
        }
        $modDetail = $this->getModDetailsArray($loginType);

        $userFullName = '';
        if (isset($row['first_name'])){
            $userFullName = $row['first_name'] . " " . $row['last_name'];
        } else {
            $userFullName = $row[$modDetail['titleField']];
        }

        $_SESSION['cpContactId']       = $row[$modDetail['keyField']];
        $_SESSION['cpUserNameWWW']     = isset($row['first_name']) ? $row['first_name'] : '';
        $_SESSION['cpUserNameWWW']    .= isset($row['last_name']) ? ' ' . $row['last_name'] : '';
        $_SESSION['cpEmail']           = $row['email'];
        $_SESSION['cpUserFullNameWWW'] = $userFullName;
        $_SESSION['cpIsLoggedInWWW']   = true;
        $_SESSION['cpLoginTypeWWW']    = $loginType;
        $_SESSION['student_id']        = '';
        $_SESSION['cpStatus']          = $row['status'];

        $tv['isLoggedInWWW'] = true; // used in twig

        if ($saveLogin == "1") {
            if($cpCfg['cp.useRememberMeTokenForCookieLogin']){
                $this->createRemberMeToken($row['email'], $row['pass_word']);
            } else {
                $fn->setCookie("cpWWWUserNameC", $row['email']);
                $fn->setCookie("cpWWWPasswordC", $row['pass_word']);
                $fn->setCookie("cpWWWLoginTypeC", $loginType);
            }
        } else {
            $fn->resetCookie("cpWWWUserNameC");
            $fn->resetCookie("cpWWWPasswordC");
            $fn->resetCookie("cpWWWLoginTypeC");
        }

        if($cpCfg['cp.hasLoginHistory']){
            $this->logLogin($_SESSION['cpContactId']);
        }

        $retUrl = $this->setReturnUrlAfterLogin($row);
        $fn->sessionRegenerate();

        CP_Common_Lib_Registry::arrayMerge('tv', $tv);
        return $retUrl;
    }

    /**
     *
     */
    function getLogout() {
        $cpUtil = Zend_Registry::get('cpUtil');
        $db = Zend_Registry::get('db');
        $fn = Zend_Registry::get('fn');
        $cpCfg = Zend_Registry::get('cpCfg');

        unset($_SESSION['cpContactId']);
        unset($_SESSION['cpUserNameWWW']);
        unset($_SESSION['cpEmail']);
        unset($_SESSION['cpUserFullNameWWW']);
        unset($_SESSION['cpIsLoggedInWWW']);
        unset($_SESSION['cpLoginTypeWWW']);
        unset($_SESSION['student_id']);

        $fn->resetCookie("cpWWWUserNameC");
        $fn->resetCookie("cpWWWPasswordC");

        if (isset($_SESSION['shippingDetails'])){
            unset($_SESSION['shippingDetails']);
        }

        if($cpCfg['cp.useRememberMeTokenForCookieLogin']){
            $this->deleteCurrentRememberMeToken();
            $fn->resetCookie("cpWWWRememberMeTokenC");
        }

        if($cpCfg['cp.hasLoginHistory']){
            $session_id = session_id();
            $SQL = "
            UPDATE login_history
            SET active = 0
            WHERE session_id = '{$session_id}'
            ";
            $result = $db->sql_query($SQL);
        }
        $fn->sessionRegenerate();
        $cpUtil->redirect('/');
    }
}