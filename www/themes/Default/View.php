<?
class CPL_Www_Themes_Default_View extends CP_Www_Themes_Default_View
{
    var $jssKeys = array('jqForm-2.69', 'jqUploadify3.2');
    /**
     *
     */
    function getHeaderPanel(){
        $cpCfg = Zend_Registry::get('cpCfg');
        $ln = Zend_Registry::get('ln');
        $pLogin = getCPPluginObj('member_login');
        $mainNav = getCPWidgetObj('core_mainNav');

        $text = "
        {$pLogin->view->getLoginInfoText()}
        ";

        return $text;
    }

    /**
     *
     */
    function getNavPanel(){
        $cpCfg = Zend_Registry::get('cpCfg');
        $mainNav = Zend_Registry::get('mainNav');

        $text = "
        <nav id='nav'>
            <a id='navigation' name='navigation'></a>
            {$mainNav->getWidget(array(
                 'btnPos' => 'Top'
            ))}
        </nav>
        ";

        return $text;
    }

    /**
     *
     */
    function getNavPanel2() {
        $tv = Zend_Registry::get('tv');
        $cpCfg = Zend_Registry::get('cpCfg');
        $modulesArr = Zend_Registry::get('modulesArr');
        $searchHTML = Zend_Registry::get('searchHTML');

        $langBtns = '';
        $searchText = '';
        if ($tv['action'] == 'list') {
            $searchText = $searchHTML->getSearchHTML($tv['module']);
        }

        $text = "
        <div class='navPanel'>
            <div class='floatbox'>
                <div class='float_left'>
                    {$this->getPagerPanel()}
                </div>
                <div class=''>
                    {$this->getActionButtons()}
                </div>
            </div>
            {$searchText}
        </div>
        ";

        return $text;
    }

    /**
     *
     */
    function getActionButtons(){
        $action = Zend_Registry::get('action');
        $tv = Zend_Registry::get('tv');
        $actionBtns = $action->getActionButtons();
        
        if ($actionBtns != '') {
            $actionBtns = "
            <div class='hlist actionBtns noBg'>
                {$actionBtns}
            </div>
            ";
        }

        $text = "
        {$actionBtns}
        ";

        return $text;
    }
    
    /**
     *
     */
    function getBodyPanel() {
        $tv = Zend_Registry::get('tv');
        $clsInst = Zend_Registry::get('currentModule');

        $actionName = ($tv['action']) != '' ? ucfirst($tv['action']) : 'List';
        $actionTemp  = "get{$actionName}";  //eg: getList
        if (!method_exists($clsInst, $actionTemp)) {
            $clsName = ucfirst($tv['module']);
            $error = includeCPClass('Lib', 'Errors', 'Errors');
            $exp = array(
                'replaceArr' => array(
                     'clsName' => $clsName
                    ,'funcName' => $actionTemp
                )
            );
            print $error->getError('themeMethodNotFound', $exp);
            exit();
        }

        $text = "
        <div class='bodyPanel'>
            {$clsInst->getController()}
        </div>
        ";

        return $text;
    }

    /**
     *
     */
    function getLeftPanel(){
        $tv = Zend_Registry::get('tv');
        $subNav = Zend_Registry::get('subNav');
        $clsInst = Zend_Registry::get('currentModule');

        if (method_exists($clsInst, 'getLeftPanel')) {
            $text = $clsInst->getLeftPanel();
        } else {
            $text = "
            {$subNav->getWidget()}
            ";
        }

        return $text;
    }

    /**
     *
     */
    function getRightPanel(){
        $tv = Zend_Registry::get('tv');
        $subNav = Zend_Registry::get('subNav');
        $fn = Zend_Registry::get('fn');

        $clsName = ucfirst($tv['module']);
        $modObj  = includeCPClass('Module', $tv['module'], $clsName);

        if (method_exists($modObj, 'getRightPanel')) {
            $text = $modObj->getRightPanel();
        } else {
            $text = "";
        }

        return $text;
    }

    /**
     *
     */
    function getFooterPanel(){
        $ln = Zend_Registry::get('ln');
        $text = "
        <div class='floatbox'>
            <div class='float_left'>
                {$ln->gd('cp.footer.leftText')}
            </div>
            <div class='float_right'>
                {$ln->gd('cp.footer.rightText')}
            </div>
        </div>
        ";

        return $text;
    }

    /**
     *
     */
    function getMainThemeOutput() {
        //body panel must be in the top since (in twopresents) there are some variables
        //set in here which is re-used in the left panel
        $bodyPanel       = $this->getBodyPanel();
        $headerPanel     = $this->getHeaderPanel();
        $navPanel        = $this->getNavPanel();
        $navPanel2       = '';
        $leftPanel       = $this->getLeftPanel();
        $rightPanel      = $this->getRightPanel();
        $mainBottomPanel = $this->getMainBottomPanel();
        $footerPanel     = $this->getFooterPanel();
        $extendedPanel   = $this->getExtendedPanel();
        $lastPanel       = $this->getLastPanelOutsideTemplate();

        $tv = Zend_Registry::get('tv');
        $cpCfg = Zend_Registry::get('cpCfg');
        $viewHelper = Zend_Registry::get('viewHelper');
        $pageCSSClass = $viewHelper->getPageCSSClass();
        $mainNav = getCPWidgetObj('core_mainNav');

        $banner = $this->getBannerPanel();

        $bannerInCol3 = '';
        if ($cpCfg['cp.showBannerInCol3Top'] && $tv['secType'] != 'Home'){
            $wBanner = getCPWidgetObj('media_banner');
            $bannerInCol3 = $wBanner->getWidget();
        }

        if (method_exists($this, 'getNavPanel2')) {
            $navPanel2 = $this->getNavPanel2();
            if ($navPanel2 != '') {
                $navPanel2 = "
                <nav id='nav2'>
                    {$navPanel2}
                </nav>
                ";
            }
        }

        $footerText = "
        <footer id='footer' class='clearfix ym-clearfix'>
            {$footerPanel}
        </footer>
        ";

        if($cpCfg['cp.fullWidthTemplte']){;
            $footerText = "
            <footer id='footer'>
                <a id='navigation' name='navigation'></a>
                <div class='page_margins ym-wrapper'>
                    <div class='page ym-wbox'>
                        {$footerPanel}
                    </div>
                </div>
            </footer>
            ";
        }

        $navInsideHeader = '';
        $navOutsideHeader = '';
        if($cpCfg['cp.placeNavInsideHeaderTag']){
            $navInsideHeader = $navPanel;
        } else {
            $navOutsideHeader = $navPanel;
        }

        $footerInside = '';
        $footerOutside = '';
        if($cpCfg['cp.placeFooterOutsidePageTag']){
            $footerOutside = $footerText;
        } else {
            $footerInside = $footerText;
        }

        if ($mainBottomPanel != ''){
            $mainBottomPanel = "
            <div class='mainBottom'>
                {$mainBottomPanel}
            </div>
            ";
        }

        $logoLink = $this->getLogoLink();

        $mainInner = "
        <div class='mainInner'>
            <aside id='col1' class='ym-col1'>
                <div id='col1_content' class='clearfix ym-clearfix ym-cbox-left'>
                    {$leftPanel}
                </div>
            </aside>
            <aside id='col2' class='ym-col2'>
                <div id='col2_content' class='clearfix ym-clearfix ym-cbox-right'>
                    {$rightPanel}
                </div>
            </aside>
            <div id='col3' class='ym-col3'>
                <div id='col3_content' class='clearfix ym-clearfix ym-cbox'>
                    <a id='contentMain' name='contentMain'></a>
                    {$navPanel2}
                    {$bannerInCol3}
                    {$bodyPanel}
                </div>
                <div id='ie_clearing'>&nbsp;</div>
            </div>
        </div>
        ";

        if($cpCfg['cp.fullWidthTemplte']){
            $text = "
            <header id='header'>
                <div class='page_margins ym-wrapper'>
                    <div class='page ym-wbox'>
                      {$headerPanel}
                      <a id='logo' href='{$logoLink}'><span class='hideme ym-hideme'>Logo</span></a>
                      {$navInsideHeader}
                    </div>
                </div>
            </header>
            {$navOutsideHeader}
            {$banner}
            <div id='main' class='{$pageCSSClass} clearfix ym-clearfix'>
                <div class='page_margins ym-wrapper'>
                    <div class='page ym-wbox'>
                        {$mainInner}
                    </div>
                </div>
            </div>
            <div id='extended'>
                <div class='page_margins ym-wrapper'>
                    <div class='page ym-wbox'>
                        {$extendedPanel}
                    </div>
                </div>
            </div>
            {$footerInside}
            {$lastPanel}
            ";
        } else {
            $text = "
            <div id='page_margins' class='page_margins ym-wrapper'>
                <div class='page ym-wbox'>
                    <header id='header'>
                        {$headerPanel}
                        <a id='logo' href='{$logoLink}'><span class='hideme ym-hideme'>Logo</span></a>
                        {$navInsideHeader}
                    </header>
                    {$navOutsideHeader}
                    {$mainNav->getWidget(array(
                          'btnPos'  => 'Top Most'
                         ,'class'   => 'hlist topMost'
                    ))}
                    {$banner}
                    <div id='main' class='{$pageCSSClass} clearfix ym-clearfix'>
                        {$mainInner}
                        {$mainBottomPanel}
                    </div>
                    {$footerInside}
                </div>
                {$footerOutside}
            </div>
            {$lastPanel}
            ";
        }

        $themeObj = Zend_Registry::get('currentTheme');

        if (method_exists($themeObj, 'init')) {
            $themeObj->init();
        }

        return $text;
    }
}