<?
class CPL_Www_Widgets_Core_MainNav_Model extends CP_Www_Widgets_Core_MainNav_Model
{
    /**
     *
     */
    function getDataArray() {
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpCfg = Zend_Registry::get('cpCfg');

        $modelHelper = Zend_Registry::get('modelHelper');
        $dataArray = $modelHelper->getWidgetDataArray($this->controller, 'core_mainNav');

        $arr = array();
        foreach ($dataArray as $row){
            $tmpArr = &$arr[$row['section_id']];
            $tmpArr['section_id']        = $row['section_id'];
            $tmpArr['title']             = $ln->gfv($row, 'title');
            $tmpArr['titleEng']          = $row['title'];
            $tmpArr['titleActual']       = $ln->gfv($row, 'title', false);
            $tmpArr['button_position']   = $row['button_position'];
            $tmpArr['section_type']      = $row['section_type'];
            $tmpArr['url']               = $this->getUrl($row);
            $tmpArr['external_link']     = $row['external_link'];
            $tmpArr['internal_link']     = $row['internal_link'];
            $tmpArr['show_in_nav']       = $row['show_in_nav'];
            $tmpArr["module"]            = $fn->getModuleNameByType($row['section_type']);
            $tmpArr['member_only']       = isset($row['member_only']) ? $row['member_only'] : 0;
            $tmpArr['non_member_only']   = isset($row['non_member_only']) ? $row['non_member_only'] : 0;

            $tmpArr['access_to_student'] = $row['access_to_student'];
            $tmpArr['access_to_teacher'] = $row['access_to_teacher'];
            $tmpArr['access_to_parent']  = $row['access_to_parent'];
            $tmpArr['access_to_public']  = $row['access_to_public'];

            if($dbUtil->getColumnExists('section', 'css_style_name')){
                $tmpArr['css_style_name'] = $row['css_style_name'];
            }

            if($dbUtil->getColumnExists('section', 'caption')){
                $tmpArr['caption'] = $row['caption'];
            }

            if($cpCfg['cp.hasAliasSectionsAcrossMUSites']){
                $tmpArr['alias_to_section_id'] = $row['alias_to_section_id'];
            }
        }

        $this->dataArray = $arr;
        return $this->dataArray;
    }
}