<?
class CPL_Www_Widgets_Core_MainNav_View extends CP_Www_Widgets_Core_MainNav_View
{
    /**
     *
     */
    function isSkipRecord($row, $btnPos) {
        $cpCfg = Zend_Registry::get('cpCfg');
        $theme = Zend_Registry::get('currentTheme');

        $section_type = $row['section_type'];
        
        $skip = false;
        
        if ($row['show_in_nav'] == 0 || $btnPos != $row['button_position']){
            $skip = true;
        }


        if ($theme->isLoggedInStudent() && $row['access_to_student'] == 0){
            $skip = true;
        }

        if ($theme->isLoggedInTeacher() && $row['access_to_teacher'] == 0){
            $skip = true;
        }

        if ($theme->isLoggedInParent() && $row['access_to_parent'] == 0){
            $skip = true;
        }

        if (!isLoggedInWWW() && $row['access_to_public'] == 0){
            $skip = true;
        }

        return $skip ;
    }
    
}