<?
class CPL_Www_Widgets_Member_LoginForm_Controller extends CP_Www_Widgets_Member_LoginForm_Controller
{

    /**
     *
     */
    function getSendMessageToAdmin() {
        return $this->view->getSendMessageToAdmin();
    }

    /**
     *
     */
    function getSendMessageToAdminSubmit() {
        return $this->view->getSendMessageToAdminSubmit();
    }

    /**
     *
     */
    function getSendMessageToAdminValidate() {
        return $this->view->getSendMessageToAdminValidate();
    }

    /**
     *
     */
    function getLoginTypeValues() {
        return $this->view->getLoginTypeValues();
    }
}