<?
class CPL_Www_Widgets_Member_LoginForm_View extends CP_Www_Widgets_Member_LoginForm_View
{
    var $jssKeys = array('jqForm-3.15');

    //========================================================//
    function getWidget() {
        $tv = Zend_Registry::get('tv');
        $ln = Zend_Registry::get('ln');

        $c = &$this->controller;

        if ($c->hasSubcols){
            $exp = array('imgStyle' => 'float_right', 'folder' => 'large');
            $wBanner = getCPWidgetObj('media_banner');
            $pic = $wBanner->getWidget(array(
                'strictToPage' => true
            ));

            $text = "
            <div class='subcolumns'>
                <div class='{$c->col1Css}'>
                    <div class='subcl'>
                        {$this->getRowsHTML()}
                    </div>
                </div>
                <div class='{$c->col2Css}'>
                    <div class='subcr'>
                        {$pic}
                    </div>
                </div>
            </div>
            ";
        } else {
            $text = "
            {$this->getRowsHTML()}
            ";
        }

        return $text;
    }

    //========================================================//
    function getRowsHTML() {
        $hook = getCPWidgetHook('member_loginForm', 'rowsHTML', $this);
        if($hook['status']){
            return $hook['html'];
        }

        $ln = Zend_Registry::get('ln');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');
        $cpUrl = Zend_Registry::get('cpUrl');
        $cpCfg = Zend_Registry::get('cpCfg');

        $c = &$this->controller;

        $formAction = $c->formAction;
        $expPass['password'] = 1;

        $infoText = $ln->gd2($c->infoText);

        if ($infoText != ''){
            $infoText = "<div class='infoText'>{$infoText}</div>";
        }

        if ($c->returnUrlAfterLogin != ''){
            $_SESSION['cpReturnUrlAfterLogin'] = $c->returnUrlAfterLogin;
        }
        $url = '/index.php?plugin=member_forgotPassword&_spAction=view&showHTML=0';
        $forgotText = "
        <div class='forgotPasswordLink'>
            <a href='javascript:void(0)' link='{$url}' class='jqui-dialog-form' formId='forgotPasswordForm'
                w='450' h='300' title='{$ln->gd('p.member.forgotPassword.form.heading')}'>
            </a>
        </div>
        ";

        $retUrlText = '';
        if ($c->returnUrl){
            $retUrlText = "<input type='hidden' name='returnUrl' value='{$c->returnUrl}' />";
        }

        $regiserInfo = '';
        if ($c->hasRegiserInfo){
            $regiserInfo = $this->getRegisterInfo();
        }

        $fbLoginText = '';
        if ($c->hasFbLogin){
            $fbLoginText = $this->getFbLoginText();
        }

        $loginType = '';
        if (is_array($c->loginTypeArr)){
            $exp = array('useKey' => true, 'hideFirstOption' => true);
            $loginType = $formObj->getDDRowByArr($ln->gd('cp.form.fld.loginType'), 'loginType', $c->loginTypeArr, $c->loginType, $exp);
        } else {
            $loginType = "<input type='hidden' name='loginType' value='{$c->loginType}' />";
        }

        $emailLink = '/index.php?widget=member_loginForm&_spAction=sendMessageToAdmin&showHTML=0';

        $saveLogin = "
        <div class='type-check saveLogin'>
            <input type='checkbox' checked='checked' id='fld_save_login' class='checkBox' name='saveLogin' value='1' />
            <label for='fld_save_login'>Remember My Password</label>
        </div>
        ";

        $urlreset = '/index.php?plugin=member_resetPassword&_spAction=view&showHTML=0';
        $resetPassword1 = "
        <div class='resetPasswordLink'>
            <a href='javascript:void(0)' link='{$urlreset}' class='jqui-dialog-form' formId='resetPasswordForm'
                w='450' h='300' title='Reset Password'>Reset Password
            </a>
        </div>
        ";

        $studentBtns = '';
        if($cpCfg['cp.primarySchool'] == 1) {
            $studentBtns ="
            <div><a class='loginTypeStudent' value='edukite_student'><img src='/cmspilotv30/CP/www/themes/Manager/images/login/studentA.png'></a></div>
            ";
        }

        $loginTypeBtns = "
        <div class='loginType'>
            {$studentBtns}
            <div><a class='loginTypeParent' value='edukite_parent'><img src='/cmspilotv30/CP/www/themes/Manager/images/login/parentA.png'></a></div>
            <div><a class='loginTypeTeacher' value='edukite_teacher'><img src='/cmspilotv30/CP/www/themes/Manager/images/login/teacherA.png'></a></div>
        </div>
        ";

        $archiveLink = "http://www.loquatvalleyarchive.edukite.com";

        $text = "
        <form name='loginForm' id='loginForm' class='img-responsive yform columnar cpJqForm' method='post' action='{$c->formAction}'>
            <div class='loginImage'><img src='/cmspilotv30/CP/www/themes/Manager/images/login/logo.png'></div>
            {$loginTypeBtns}
            <fieldset>
                {$infoText}
                {$formObj->getTextBoxRow($ln->gd("<img src='/cmspilotv30/CP/www/themes/Manager/images/login/username.png'>"), 'email')}
                {$formObj->getTextBoxRow($ln->gd("<img src='/cmspilotv30/CP/www/themes/Manager/images/login/username.png'>"), 'username')}
                {$formObj->getTextBoxRow($ln->gd("<img src='/cmspilotv30/CP/www/themes/Manager/images/login/password.png'>"), 'pass_word', '', $expPass)}
                <div class='type-button'>
                    <div class='floatbox'>
                        <div class='btns'>
                            <input type='submit' value=''/>
                        </div>
                    </div>
                </div>
                <input type='hidden' name='siteType' value='{$tv['siteType']}' />
                <input type='hidden' name='loginType' value='edukite_teacher' />
                <input type='submit' name='x_submit' class='submithidden' />
                <div class='floatbox'>
                    <div class='float_left'>
                        <div class='emailLink'>
                            <a href='javascript:void(0)' link='{$emailLink}' class='jqui-dialog-form' formId='messageToAdminForm'
                                w='450' h='300' title='{$ln->gd('w.member.loginForm.link.messageToAdmin')}'>
                                <img src='/cmspilotv30/CP/www/themes/Manager/images/login/contactUsB.png'>
                            </a>
                        </div>
                    </div>
                {$saveLogin}
                </div>
                {$retUrlText}
                {$resetPassword1}
            </fieldset>
            <div class='archiveEnter'><a href='{$archiveLink}'><img src='/cmspilotv30/CP/www/themes/Manager/images/login/archiveEnterB.png'></a></div>
        </form>
        {$regiserInfo}
        {$fbLoginText}
        <div class = 'facebookPanel'>
            <iframe src='http://www.facebook.com/plugins/likebox.php?id=124503690986&amp;show_faces=false&amp;connections=20&amp;width=129&amp;stream=false&amp;border_color=%23ffffff&amp;header=false&amp;height=55' scrolling='no' frameborder='0' border='0' style='border:none; overflow:auto; width:129px; height:70px;' allowTransparency='true'></iframe>
        </div>
        ";

        return $text;
    }

    //========================================================//
    function getRegisterInfo() {
        $hook = getCPWidgetHook('member_loginForm', 'registerInfo', $this);
        if($hook['status']){
            return $hook['html'];
        }

        $ln = Zend_Registry::get('ln');
        $cpUrl = Zend_Registry::get('cpUrl');

        $c = &$this->controller;

        $formAction = $c->formAction;
        $expPass['password'] = 1;

        $url = ($c->registerUrl != '') ? $c->registerUrl : $cpUrl->getUrlBySecType('Register');

        $text = "
        <div class='registerInfo'>
            <form class='yform'>
            <fieldset>
                <h1>{$ln->gd($c->registerCaption)}</h1>
                <div class='infoText'>{$ln->gd($c->registerInfoText)}</div>
                <div class='type-button'>
                    <a class='button btnRegister' href='{$url}'>{$ln->gd($c->registerBtnText)}</a>
                </div>
            </fieldset>
            </form>
        </div>
        ";

        return $text;
    }

    //========================================================//
    function getFbLoginText() {
        $hook = getCPWidgetHook('member_loginForm', 'fbLoginText', $this);
        if($hook['status']){
            return $hook['html'];
        }

        $ln = Zend_Registry::get('ln');

        $c = &$this->controller;

        $fbLoginCaption = $ln->gd2($c->fbLoginCaption);
        $fbLoginInfoText = $ln->gd2($c->fbLoginInfoText);

        if ($fbLoginCaption != ''){
            $fbLoginCaption = "<h1>{$ln->gd($c->fbLoginCaption)}</h1>";
        }

        if ($fbLoginInfoText != ''){
            $fbLoginInfoText = "<div class='infoText'>{$fbLoginInfoText}</div>";
        }

        $text = "
        <div class='facebookLogin'>
            {$fbLoginCaption}
            {$fbLoginInfoText}
            <div class='fbLoginButton'>
                <a href='#'></a>
            </div>
        </div>
        ";

        return $text;
    }

    /**
     *
     */
    function getSendMessageToAdmin() {
        $ln = Zend_Registry::get('ln');
        $formObj = Zend_Registry::get('formObj');

        $formAction = '/index.php?widget=member_loginForm&_spAction=sendMessageToAdminSubmit&showHTML=0';
        $returnUrl = "/eng/login/";

        $text = "
        <form name='messageToAdminForm' id='messageToAdminForm' class='yform columnar cpJqForm' method='post' action='{$formAction}'>
            <fieldset>
                {$formObj->getTextBoxRow('Name', 'first_name')}
                {$formObj->getTextBoxRow($ln->gd('cp.form.fld.email.lbl'), 'email')}
                {$formObj->getTextAreaRow($ln->gd('cp.form.fld.message.lbl'), 'comments')}
                <input type='hidden' name='dialogMessage' value='{$ln->gd('p.member.emailToAdmin.form.message.success')}' />
                <input type='submit' name='x_submit' class='submithidden' />
                <input type='hidden' name='returnUrl' value='{$returnUrl}' />
            </fieldset>
        </form>
        ";

        return $text;
    }

    /**
     *
     */
    function getSendMessageToAdminSubmit() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpCfg = Zend_Registry::get('cpCfg');
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $tv = Zend_Registry::get('tv');
        $validate = Zend_Registry::get('validate');

        if (!$this->getSendMessageToAdminValidate()){
            return $validate->getErrorMessageXML();
        }

        //-----------------------------------------------------------------------//
        $fa = array();

        $fa['first_name']    = $fn->getPostParam('first_name');
        $fa['email']         = $fn->getPostParam('email');
        $fa['comments']      = $fn->getPostParam('comments');
        $fa['creation_date'] = date('Y-m-d H:i:s');

        //-----------------------------------------------------------------//
        $currentDate  = date('d-M-Y l h:i:s A');
        $hostName     = $_SERVER['HTTP_HOST'];

        $message = $ln->gd('w.member.emailToAdmin.form.enquiry.notifyBody');
        $message = str_replace('[[first_name]]', $fa['first_name'], $message);
        $message = str_replace('[[email]]', $fa['email'], $message);
        $message = str_replace('[[comments]]', $fa['comments'], $message);
        $message = str_replace('[[currentDate]]', $currentDate, $message);
        $message = str_replace('[[url]]', $hostName, $message);

        $subject   = $ln->gd('w.member.emailToAdmin.form.enquiry.notifySubject');
        // $fromName  = $fa['first_name'] . ' ' . $fa['last_name'];
        $fromName  = '';
        $fromEmail = $fa['email'];
        $firstName = $fa['first_name'];
        $toName    = $cpCfg['cp.companyName'];
        $toEmail   = $cpCfg['cp.adminEmail'];
        //$toEmail   = 'moin@usoftsolutions.com';

        $args = array(
             'toName'    => $toName
            ,'toEmail'   => $toEmail
            ,'subject'   => $subject
            ,'message'   => $message
            ,'firstName' => $firstName
            ,'fromName'  => $fromName
            ,'fromEmail' => $fromEmail
        );


        $emailMsg = includeCPClass('Lib', 'EmailTemplate', 'EmailTemplate', true, array('args' => $args));
        $exp = array('showHeader' => false);
        $emailMsg->sendEmail($exp);

        return $validate->getSuccessMessageXML();
    }

    /**
     *
     */
    function getSendMessageToAdminValidate() {
        $cpCfg = Zend_Registry::get('cpCfg');
        $ln = Zend_Registry::get('ln');
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');

        //==================================================================//
        $validate->resetErrorArray();
        $validate->validateData("first_name", "Please enter your name");
        $validate->validateData("email", $ln->gd("cp.form.fld.email.err"), "email");
        $validate->validateData("comments", $ln->gd("cp.form.fld.comments.err"));

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    function getLoginTypeValues() {
        $fn = Zend_Registry::get('fn');

        $activeLayout  = $fn->getReqParam('activeLayout');

        $_SESSION['loginButton'] = $activeLayout;

    }
}