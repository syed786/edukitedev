Util.createCPObject('cpw.member.loginForm');

cpw.member.loginForm.init = function(){

    //--------------------------- TO SHOW LOGIN TYPE BUTTONS DEFAULT ACTIVE -----------------------------
    $(window).load(function(){

        $(".loginTypeStudent img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/studentA.png');
        $(".loginTypeParent img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/parentA.png');
        $(".loginTypeTeacher img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/teacherB.png');

    });

    //-----------------------------------------------------------------------
    $('#contactLink').livequery('click', function(){
        $('#dialog').dialog('close');
        $('#dialog1').dialog('close');
    });

    $("a.loginTypeStudent").click(function() {
        var activeLayout = $('.loginTypeStudent').attr('value');
        $('input[name=loginType]').val(activeLayout);

        $(".loginTypeStudent img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/studentB.png');
        $(".loginTypeParent img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/parentA.png');
        $(".loginTypeTeacher img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/teacherA.png');
        $('#loginForm .row_email').hide();
        $('#loginForm .row_username').show();

    });

    $("a.loginTypeParent").click(function() {
        var activeLayout = $('.loginTypeParent').attr('value');
        $('input[name=loginType]').val(activeLayout);

        $(".loginTypeParent img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/parentB.png');
        $(".loginTypeStudent img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/studentA.png');
        $(".loginTypeTeacher img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/teacherA.png');
        $('#loginForm .row_email').show();
        $('#loginForm .row_username').hide();
    });

    $("a.loginTypeTeacher").click(function() {
        var activeLayout = $('.loginTypeTeacher').attr('value');
        $('input[name=loginType]').val(activeLayout);

        $(".loginTypeTeacher img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/teacherB.png');
        $(".loginTypeStudent img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/studentA.png');
        $(".loginTypeParent img").attr('src', '/cmspilotv30/CP/www/themes/Manager/images/login/parentA.png');
        $('#loginForm .row_email').show();
        $('#loginForm .row_username').hide();

    });

}
