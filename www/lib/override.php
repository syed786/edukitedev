<?
$cpCfg = Zend_Registry::get('cpCfg');
$tv = Zend_Registry::get('tv');
$fn = Zend_Registry::get('fn');
$cpUtil = Zend_Registry::get('cpUtil');

if (($tv['secType'] != 'Login' && !isLoggedInWWW())
    && $tv['spAction'] != 'loginSubmit' && $tv['spAction'] != 'sendParentEmailAlert'
    && $tv['spAction'] != 'sendMessageToAdminSubmit'
    && $tv['spAction'] != 'sendMessageToAdmin'
    && $tv['spAction'] != 'submit'
    && $tv['spAction'] != 'view'
    && $tv['spAction'] != 'resetSubmit'
    && $tv['secType'] != 'Reset Password'
    ){
    $login = getCPPluginObj('member_login', false);
    $login->model->loginWithCookie();//this is used for save login
    checkLoggedIn();
}

$link = $_SERVER['REQUEST_URI'];
if($link == '/controller/home/'){
    $pLogin = getCPPluginObj('member_login');
    return $pLogin->model->getLogout();
}


if(!isLoggedInWWW()){
	$tv['siteType'] = ($tv['siteType'] != '') ? $tv['siteType'] : 'controller';
} else{
	$tv['siteType'] = ($tv['siteType'] != '') ? $tv['siteType'] : 'kite';
}

if ($tv['siteType'] == 'kite'){
    $cpCfg['cp.theme'] = 'Kite';
}

$cssFilesArr = array();
$cssFilesArr[] = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css';
$cssFilesArr[] = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css';

CP_Common_Lib_Registry::arrayMerge('tv', $tv);
CP_Common_Lib_Registry::arrayMerge('cpCfg', $cpCfg);
CP_Common_Lib_Registry::arrayMerge('jssKeys', array('uploadifive-1.1.2'));
CP_Common_Lib_Registry::arrayMerge('cssFilesArr', $cssFilesArr);
