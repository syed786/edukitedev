<?
$cpCfg = array();

$cpCfg['m.membership.returnUrlAfterLoginSecRecType'] = 'Student';
$cpCfg['m.edukite.current_academic_year']  = date('Y');
$cpCfg['cp.viewRecType.home.pageCSSClass'] = 'hideboth';
$cpCfg['cp.sendFeedbackEmailToTeacher'] = 1;
$cpCfg['cp.primarySchool'] = 0 ;
$cpCfg['showAcheivement'] = 1;
$cpCfg['cp.parentFeedbackinTeacher'] = 1;
$cpCfg['cp.parentFeedbackinTeacher'] = 1;
$cpCfg['cp.galleryPdfExportinRight'] = 1;
$cpCfg['cp.noticeReadSummary'] = 1;
$cpCfg['cp.assetVersion'] = '100';

return $cpCfg;