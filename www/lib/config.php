<?
$cpCfg = array();
$cpCfg['cp.theme'] = 'Manager';
$cpCfg['cp.useMinFiles'] = false;
$cpCfg['cp.defaultPageCssClass'] = '';
$cpCfg['cp.showActionButtonsBelowNewForm'] = false;
$cpCfg['cp.multiLang'] = 0;
$cpCfg['cp.jqVersion']   = '1.9.1';
$cpCfg['cp.jqUiVersion'] = '1.9.2';
$cpCfg['cp.yamlVersion'] = 'v4.0.2';

$modArr = array(
     'webBasic_home'
    ,'webBasic_section'
    ,'webBasic_content'
    ,'webBasic_contactUs'
    ,'membership_contact'
    ,'edukite_student'
    ,'edukite_parent'
    ,'edukite_teacher'
    ,'edukite_class'
    ,'edukite_subject'
    ,'edukite_task'
    ,'edukite_notice'
    ,'edukite_calendar'
    ,'edukite_teacherNotice'
    ,'edukite_yearGroup'
    ,'edukite_interest'
    ,'edukite_yearGroup'
    ,'edukite_notice'
    ,'edukiteWeb_notice'
    ,'edukite_type'
    ,'edukite_achievement'
);

$hiddenModules = array(
);

$cpCfg['cp.availableModules'] = array_merge($modArr, $hiddenModules);

$cpCfg['cp.availableModGroups'] = array(
     'webBasic'
    ,'membership'
    ,'edukite'
    ,'edukiteWeb'
);

$cpCfg['cp.availableWidgets'] = array(
     'core_mainNav'
    ,'core_subNav'
    ,'core_subCat'
    ,'media_anythingSlider'
    ,'media_imagesSlider'
    ,'media_bsCarousel'
    ,'content_record'
    ,'member_loginForm'
    ,'edukite_calendarDisplay'
);

$cpCfg['cp.availablePlugins'] = array(
     'common_comment'
    ,'common_media'
    ,'common_siteSearch'
    ,'member_login'
    ,'member_forgotPassword'
    ,'member_resetPassword'
);

$cpCfg['cp.moduleNamesByTypeArr'] = array(
     'Home'         => 'webBasic_home'
    ,'Content'      => 'webBasic_content'
    ,'Contact Us'   => 'webBasic_contactUs'
    ,'Site Search'  => 'webBasic_content'
    ,'Enquiry Form' => 'webBasic_contactUs'
    ,'Login'        => 'membership_contact'

    // TOP MOST
    ,'Student'      => 'edukite_student'
    ,'Parent'       => 'edukite_parent'
    ,'Teacher'      => 'edukite_teacher'
    ,'Class'        => 'edukite_class'
    ,'Subject'      => 'edukite_subject'
    ,'Year Group'   => 'edukite_yearGroup'
    ,'Interest'     => 'edukite_interest'
    ,'Type'         => 'edukite_type'
    ,'Achievement'  => 'edukite_achievement'
    ,'Daily Activity'  => 'edukite_dailyActivity'

    // TOP
    ,'Staff Notice' => 'edukite_teacherNotice'
    ,'Notice'       => 'edukite_notice'
    ,'Task'         => 'edukite_task'
    ,'Portfolio'    => 'edukite_task'
    ,'Calendar'     => 'edukite_calendar'
    ,'Kite Notice'  => 'edukiteWeb_notice'
    ,'Reset Password'  => 'membership_contact'
);

//$cpCfg['cp.viewRecType.student.list.pageCSSClass'] = 'hidecol2';

return $cpCfg;