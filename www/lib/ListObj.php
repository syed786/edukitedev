<?
/**
 *
 */
class CPL_Www_Lib_ListObj extends CP_Common_Lib_ListObj
{

    //==================================================================//
    function getGoToDetailText($rowCounter, $title, $cellID = "", $width ="", $row = array()) {
        $cpCfg = Zend_Registry::get('cpCfg');
        $pager = Zend_Registry::get('pager');
        $tv = Zend_Registry::get('tv');
        $modulesArr = Zend_Registry::get('modulesArr');
        $titleText = "";

        $width  = $width  != "" ? "width={$width}"   : "";
        $cellID = $cellID != "" ? "cellID={$cellID}" : "";

        $title = $title != "" ? $title : "---";

        if ($modulesArr[$tv['module']]['hasEditInList'] == 1) {
            $titleText = $pager->getGoToEditText($title, $rowCounter, '', $row);
        } else {
            $titleText = "";
        }

        $text = "
        <td {$width} {$cellID}>
            {$titleText}
        </td>
        ";

        return $text;
    }


    //==================================================================//
    function getListHeader($extraParam = array()) {

        $noScrollableTable = isset($extraParam['noScrollableTable']) ? $extraParam['noScrollableTable'] : false;

        $scrollableTableText = "scrollabletable='1'";
        if ($noScrollableTable) {
            $scrollableTableText = '';
        }

        $text = "
        <table class='list' {$scrollableTableText} id='bodyList' cellspacing='1'>
            <thead>
            <tr>
        ";

        return $text;
    }

    //==================================================================//
    function getListRowHeader($row, $rowCounter, $listRowClass = '', $extraParam = array()) {
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $cpCfg = Zend_Registry::get('cpCfg');
        $modulesArr = Zend_Registry::get('modulesArr');

        $hasFlagInList     = $modulesArr[$tv['module']]['hasFlagInList'];
        $hasCheckboxInList = ($modulesArr[$tv['module']]['hasCheckboxInList'] && $cpCfg['cp.hasCheckboxInList']) ? true : 0;

        $hasCheckboxInList = isset($extraParam['hasCheckboxInList']) ? $extraParam['hasCheckboxInList'] : $hasCheckboxInList;
        $keyFieldValue     = isset($extraParam['keyFieldValue'])     ? $extraParam['keyFieldValue']     : $row[$modulesArr[$tv['module']]['keyField']];

        $hasFlagInList     = isset($extraParam['hasFlagInList'])     ? $extraParam['hasFlagInList']     : $hasFlagInList;
        $hasFlagInListBlue = isset($extraParam['hasFlagInListBlue']) ? $extraParam['hasFlagInListBlue'] : 0;
        $hasFlagInListGreen = isset($extraParam['hasFlagInListGreen']) ? $extraParam['hasFlagInListGreen'] : 0;

        $hasRowNumber  = isset($extraParam['hasRowNumber'])  ? $extraParam['hasRowNumber']  : true;
        $hasEditInList = isset($extraParam['hasEditInList']) ? $extraParam['hasEditInList'] : true;

        $text = '';
        $listRowClass  = $fn->getRowClass($rowCounter%2, $keyFieldValue, $listRowClass);

        //--------------------------------------------------------------//
        $rowNumberText = '';
        $exrtaCells = '';
        if ($tv['action'] != 'print') {
            if ($hasCheckboxInList == 1) {
                $exrtaCells .= $this->getListCheckBox($rowCounter, $keyFieldValue, $listRowClass);
            }

            $hasEditAccess = true;
            if (CP_SCOPE == 'admin' && $cpCfg['cp.hasAccessModule']){
                $hasEditAccess = getCPModuleObj('core_userGroup')->model->hasAccessToAction($tv['module'], 'edit');
            }

            if ($hasEditInList && $hasEditAccess) {
                $exrtaCells .= $this->getEditIcon($rowCounter, $row);
            }

            $color = $fn->getReqParam('color', 'red');
            $flag_fld = 'flag';
            if ($color != 'red') {
                $flag_fld = 'flag_' . $color;
            }
            if ($hasFlagInList == 1) {
                $exrtaCells .= $this->getListFlagImage($row['flag'], $keyFieldValue, 'red');
            }
            if ($hasFlagInListBlue == 1) {
                $exrtaCells .= $this->getListFlagImage($row['flag_blue'], $keyFieldValue, 'blue');
            }
            if ($hasFlagInListGreen == 1) {
                $exrtaCells .= $this->getListFlagImage($row['flag_green'], $keyFieldValue, 'green');
            }

        }

        if ($hasRowNumber) {
            $rowNumberText = "
            {$this->getListDataCell($rowCounter + 1, "left", "listRowIndex__" . $keyFieldValue, 5)}
            ";
        }

        $text = "
        <tr class='{$listRowClass}' id='listRow__{$keyFieldValue}'>
        ";

        return $text;
    }
}

