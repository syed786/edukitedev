<?
class CPL_Www_Lib_FormObj extends CP_Www_Lib_FormObj
{
    /**
     *
     */
    function getUploadifyObj1($module, $recordType, $id, $exp = array()){
        $cpCfg = Zend_Registry::get('cpCfg');
        $media = Zend_Registry::get('media');
        $fn = Zend_Registry::get('fn');
        $cpUtil = Zend_Registry::get('cpUtil');

        $media->model->setMediaArray($module, $recordType);
        $mediaArray = Zend_Registry::get('mediaArray');

        $lang = $fn->getIssetParam($exp, 'lang');
        $hideUploadBtn  = $fn->getIssetParam($exp, 'hideUploadBtn', false);
        $formSuccessMsg = $fn->getIssetParam($exp, 'formSuccessMsg', false);
        $browseButtonImg = $fn->getIssetParam($exp, 'browseButtonImg', $cpCfg['cp.masterImagesPathAlias'].'icons/btn_browse.png');
        $uploadeButtonImg = $fn->getIssetParam($exp, 'uploadButtonImg', $cpCfg['cp.masterImagesPathAlias'].'icons/btn_upload_files1.png');
        $btnWidth = $fn->getIssetParam($exp, 'btnWidth', 90);
        $btnHeight = $fn->getIssetParam($exp, 'btnHeight', 20);
        $fileSizeLimit = $fn->getIssetParam($exp, 'fileSizeLimit', $cpCfg['cp.maxUploadLimit']);
        $queueSizeLimit = $fn->getIssetParam($exp, 'queueSizeLimit', 150);
        $fileTypeExts = $mediaArray[$module][$recordType]['fileTypeExts'];

        $sessionID = session_id();

        $uploadBtn = '';

        if (!$hideUploadBtn){
            $uploadBtn = "
            <div class='uploadFiles'>
                <a class='uploadQueue' href=''>UPLOAD FILES</a>
            </div>
            ";
        }

        if (CP_SCOPE == 'admin' && $cpUtil->isIEBrowser()){
            $text = "
            <form id='uploadWrapHtml_{$recordType}' class='uploadWrapHtml' method='post'
                action='/index.php?plugin=common_media&_spAction=addMedia&showHTML=0'
                enctype='multipart/form-data' name='mediaUploadHtml'>
                <table width='100%' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td>
                            <input name='fileName' type='file' />
                        </td>
                    </tr>

                    <tr>
                        <td height='40'>
                            <input class='button' type='submit' value='Upload' name='fileUpload' />
                        </td>
                    </tr>
                </table>
                <input type='hidden' name='room'       value='{$module}' />
                <input type='hidden' name='recordType' value='{$recordType}' />
                <input type='hidden' name='id'         value='{$id}' />
                <input type='hidden' name='sessionIDCP' value='{$sessionID}' />
                <input type='hidden' name='successText' value='successText' />
                <input type='hidden' name='lang' value='{$lang}' />
            </form>
            ";
        } else {
            $text = "
            <div id='uploadWrap_{$recordType}' class='uploadWrap'>
                <div id='fileQueueMedia_{$recordType}' class='fileQueueMedia'></div>
                <div class='floatbox'>
                    <div class='float_left mr10'>
                        <input type='file' name='uploadifyMedia_{$recordType}' id='uploadifyMedia_{$recordType}' />
                    </div>
                    {$uploadBtn}
                </div>
            </div>
            <script>
                $(function() {
                    var exp = {
                          formSuccessMsg: '{$formSuccessMsg}'
                        , browseButtonImg: '{$browseButtonImg}'
                        , btnWidth: '{$btnWidth}'
                        , btnHeight: '{$btnHeight}'
                        , fileSizeLimit: '{$fileSizeLimit}'
                        , queueSizeLimit: '{$queueSizeLimit}'
                        , fileTypeExts: '{$fileTypeExts}'
                        , lang: '{$lang}'
                    }
                    cpp.common.media.setUploadify('{$module}', '{$recordType}', '{$id}', '{$sessionID}', exp);
                });
            </script>
            ";
        }

        return $text;
    }
}
