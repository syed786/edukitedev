<? 

$LANGARR = array();

$LANGARR['cp.actionButton.lbl.apply'] = "Apply";
$LANGARR['cp.actionButton.lbl.cancel'] = "Cancel";
$LANGARR['cp.actionButton.lbl.delete'] = "Delete";
$LANGARR['cp.actionButton.lbl.edit'] = "Edit";
$LANGARR['cp.actionButton.lbl.new'] = "New";
$LANGARR['cp.actionButton.lbl.save'] = "Save";
$LANGARR['cp.contactSchool'] = "<b>Basekite Australia PTY LTD</b> <br> PO BOX 66<br> Fremantle WA<br> Tel: 08 9339 0060<br> email: <a href='mailto:info@edukite.com'>info@edukite.com</a>";
$LANGARR['cp.footer.leftText'] = "www.edukite.com";
$LANGARR['cp.footer.rightText'] = "Email us at info@edukite.com";
$LANGARR['cp.form.btn.cancel'] = "Cancel";
$LANGARR['cp.form.btn.submit'] = "Submit";
$LANGARR['cp.form.fld.comments.err'] = "Please enter the comments";
$LANGARR['cp.form.fld.email.err'] = "Please enter the valid email address";
$LANGARR['cp.form.fld.email.err.notFound'] = "Entered email is not in our system";
$LANGARR['cp.form.fld.email.lbl'] = "Email";
$LANGARR['cp.form.fld.fullName.lbl'] = "Full Name";
$LANGARR['cp.form.fld.loginType'] = "Login as";
$LANGARR['cp.form.fld.message.lbl'] = "Your Message";
$LANGARR['cp.form.fld.password.err'] = "Please enter the password";
$LANGARR['cp.form.fld.password.lbl'] = "Password";
$LANGARR['cp.form.fld.username.lbl'] = "Username";
$LANGARR['cp.form.lbl.pleaseSelect'] = "Please Select";
$LANGARR['cp.lbl.add'] = "Add";
$LANGARR['cp.lbl.backToList'] = "< back to list";
$LANGARR['cp.lbl.close'] = "Close";
$LANGARR['cp.lbl.keywordSearch'] = "Keyword Search";
$LANGARR['cp.pager.lbl.totalRecords'] = "total";
$LANGARR['cp.pager.next'] = "Next";
$LANGARR['cp.pager.previous'] = "Prev";
$LANGARR['m.edukiteWeb.notice.form.parentFeedback.email.notifyBody'] = "Dear [[teacher_name]],<br><br>
Please note that the following Parent feedback has been sent in response to your notice titled: [[notice_title]]
for the student: [[student_name]].<br><br>

<i style='color:blue'>[[comments]]</i><br><br>

To view the feedback in the kite, Please click the link <a href='[[site_url]]'><u>[[site_title]]</u></a> using your user name and password.<br><br>

Best Regards,<br>
Edukite Admin
";
$LANGARR['p.member.emailToAdmin.form.enquiry.notifyBody'] = "<table>

<tr>
    <td colspan='2'><u><b>Web Enquiry - Email</b></u></td>
</tr>

<tr>
   <td>Full Name</td>
   <td>[[first_name]]</td>
</tr>

<tr>
   <td>Student Name</td>
   <td>[[student_name]]</td>
</tr>

<tr>
   <td>E-mail Address</td>
   <td>[[email]]</td>
</tr>

<tr>
   <td>Comments</td>
   <td>[[comments]]</td>
</tr>

<tr>
   <td>School Name</td>
   <td>[[school_name]]</td>
</tr>

<tr>
   <td>Submitted On</td>
   <td>[[currentDate]]</td>
</tr>

</table>
";
$LANGARR['p.member.emailToAdmin.form.message.success'] = "Email Sent Successfully";
$LANGARR['p.member.forgetPassword.form.email.notifyUserBody'] = "
<table>

<tr>
    <td colspan=\"2\"><u><b>Forgot Password - Email</b></u></td>
</tr>

<tr>
   <td>First Name</td>
   <td>[[first_name]]</td>
</tr>

<tr>
   <td>Last Name</td>
   <td>[[last_name]]</td>
</tr>

<tr>
   <td>E-mail Address</td>
   <td>[[email]]</td>
</tr>

<tr>
   <td>Password</td>
   <td>[[pass_word]]</td>
</tr>

<tr>
   <td>Submitted On</td>
   <td>[[currentDate]]</td>
</tr>

</table>";
$LANGARR['p.member.forgetPassword.form.email.notifyUserSubject'] = "Edukite Forgot Password Retrieval";
$LANGARR['p.member.forgotPassword.form.heading'] = "Recover Password";
$LANGARR['p.member.forgotPassword.form.message.success'] = "Password Sent Successfully";
$LANGARR['p.member.login.form.err.invalidLogin'] = "Please enter a valid email & password";
$LANGARR['p.member.login.lbl.welcome'] = "Welcome";
$LANGARR['p.member.resetPassword.form.email.notifyUserBody'] = "Dear [[first_name]] [[last_name]],<br><br>
Please click the following link to reset your password: [[reset_password_link]]
for your email: [[email]].<br><br>

Best Regards,<br>
Edukite Admin
";
$LANGARR['p.member.resetPassword.form.email.notifyUserSubject'] = "Reset Password";
$LANGARR['w.member.emailToAdmin.form.enquiry.notifyBody'] = "<table>

<tr>
    <td colspan=\"2\"><u><b>Web Enquiry - Email</b></u></td>
</tr>

<tr>
   <td>Full Name</td>
   <td>[[first_name]]</td>
</tr>

<tr>
   <td>E-mail Address</td>
   <td>[[email]]</td>
</tr>

<tr>
   <td>Comments</td>
   <td>[[comments]]</td>
</tr>

<tr>
   <td>School URL</td>
   <td>[[url]]</td>
</tr>

<tr>
   <td>Submitted On</td>
   <td>[[currentDate]]</td>
</tr>

</table>";
$LANGARR['w.member.emailToAdmin.form.enquiry.notifySubject'] = "Enquiry for Edukite";
$LANGARR['w.member.loginForm.form.lbl.forgotPassword'] = "Forgot Password?";
$LANGARR['w.member.loginForm.form.lbl.login'] = "Login";
$LANGARR['w.member.loginForm.heading'] = "Login";
$LANGARR['w.member.loginForm.lbl.saveLogin'] = "Save Login";
$LANGARR['w.member.loginForm.link.messageToAdmin'] = "Send a Message to Edukite Admin";

/*** FROM VALUE LIST TABLE ***/
$LANGARR['Male'] = "Send a Message to Edukite Admin";
$LANGARR['Female'] = "Send a Message to Edukite Admin";
