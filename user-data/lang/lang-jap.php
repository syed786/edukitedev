<? 

$LANGARR = array();

$LANGARR['aboutMe'] = "About Me";
$LANGARR['agreeSubscribeText'] = "I agree to allow you to use the information saved in this profile to send me newletters regarding the updates in the site. <br><br>

Note: Your data will be kept confidential and will not be shared with any third party.";
$LANGARR['agreeTermsError'] = "You have to read agree the terms and conditions to proceed";
$LANGARR['agreeTermsText'] = "I have read and understood the <a class=\"nyroModal\" href=\"index.php?_spAction=spContent&ct=terms\">terms of use</a> and <a class=\"nyroModal\" href=\"index.php?_spAction=spContent&ct=privacy\">privacy statement</a>";
$LANGARR['archives'] = "Archives";
$LANGARR['areaOfStudy'] = "Area Of Study";
$LANGARR['back'] = "Back To List";
$LANGARR['blogTitle'] = "Ukindi Company Blog";
$LANGARR['by'] = "By";
$LANGARR['comments'] = "Comments";
$LANGARR['commentsError'] = "Please enter your message";
$LANGARR['companyName'] = "Company Name";
$LANGARR['country'] = "Country";
$LANGARR['countryError'] = "Please enter the country";
$LANGARR['currentLocation'] = "Current Location";
$LANGARR['dateOfBirth'] = "Date of Birth";
$LANGARR['degree'] = "Degree";
$LANGARR['displaying'] = "Displaying";
$LANGARR['education'] = "Education";
$LANGARR['email'] = "Email";
$LANGARR['emailError'] = "Please enter the email";
$LANGARR['enquiry'] = "Enquiry";
$LANGARR['enquiryEmailToAdmin'] = "<html>
<body>

<table cellpadding=\"5\" border=\"1\" align=\"left\">

<tr>
    <td colspan=\"2\"><u><b>Web Enquiry - Email</b></u></td>
</tr>

<tr>
   <td>First Name</td>
   <td>[[first_name]]</td>
</tr>

<tr>
   <td>Last Name</td>
   <td>[[last_name]]</td>
</tr>

<tr>
   <td>E-mail Address</td>
   <td>[[email]]</td>
</tr>

<tr>
   <td>Country</td>
   <td>[[country]]</td>
</tr>

<tr>
   <td>Enquiry Type</td>
   <td>[[enquiry_type]]</td>
</tr>

<tr>
   <td>Comments</td>
   <td>[[comments]]</td>
</tr>

<tr>
   <td>Submitted On</td>
   <td>[[currentDate]]</td>
</tr>

</table>

</body>
</html>";
$LANGARR['enquiryEmailToAdminSubject'] = "Web Enquiry";
$LANGARR['enquiryForm'] = "Enquiry Form";
$LANGARR['enquiryType'] = "Enquiry Type";
$LANGARR['enquiryTypeError'] = "Please select the enquiry type";
$LANGARR['enterForum'] = "Enter FAQ / Forum";
$LANGARR['enterSupport'] = "Enter Support Central";
$LANGARR['faqForum'] = "FAQ / Forum";
$LANGARR['feedbackForm'] = "Feedback Form";
$LANGARR['female'] = "Female";
$LANGARR['firstName'] = "First Name";
$LANGARR['firstNameError'] = "Please enter the first name";
$LANGARR['gender'] = "Sex";
$LANGARR['getSupport'] = "Get Support";
$LANGARR['help'] = "Help";
$LANGARR['industry'] = "Industry";
$LANGARR['jobTitle'] = "Job Title";
$LANGARR['keyword'] = "Keyword";
$LANGARR['lastName'] = "Last Name";
$LANGARR['lastNameError'] = "Please enter the last name";
$LANGARR['loginFormTitle'] = "Please login to access your account";
$LANGARR['male'] = "Male";
$LANGARR['mandatoryFieldText'] = "<span class=\"required\">All fields marked <em class=\"requiredStar\">*</em> are compulsory.</span>";
$LANGARR['memberActivationError'] = "<p>Sorry, the activation link seems to be invalid.</p>
<p>Please contact the [[administrator]]</p>";
$LANGARR['memberLogin'] = "Members Login";
$LANGARR['message'] = "Message";
$LANGARR['messageAlertMailBody'] = "<html>
<body>

<table cellpadding=\"5\" border=\"1\">

<tr>
    <td colspan=\"2\"><u><b>New message alert</b></u></td>
</tr>

<tr>
    <td colspan=\"2\">You have received a new message from a friend:</td>
</tr>

<tr>
   <td>Friend Name</td>
   <td>[[full_name]]</td>
</tr>

<tr>
   <td>User Name</td>
   <td>[[user_name]]</td>
</tr>

<tr>
   <td>Sent on</td>
   <td>[[currentDate]]</td>
</tr>


<tr>
   <td>Message details</td>
   <td>[[message]]</td>
</tr>

</table>

</body>
</html>";
$LANGARR['messageAlertMailSubject'] = "New message from your friend";
$LANGARR['more'] = "more";
$LANGARR['nationality'] = "Nationality";
$LANGARR['nationalityError'] = "Please enter the nationality";
$LANGARR['newMemberRegNotificationMailToAdmin'] = "<html>
<body>

<table cellpadding=\"5\" border=\"1\">

<tr>
    <td colspan=\"2\"><u><b>New Member Registration</b></u></td>
</tr>

<tr>
   <td>First Name</td>
   <td>[[first_name]]</td>
</tr>

<tr>
   <td>Last Name</td>
   <td>[[last_name]]</td";
$LANGARR['newMemberRegNotificationMailToAdminSubject'] = "New Member Registration";
$LANGARR['newMemberRegNotificationMailToUser'] = "<html>
<body>

<p>Dear <b>[[first_name\']] [[last_name]] </b>,</p>

<p>Welcome  to LingoLink<p>

<p>You are now registered as a member of our site. </p>

<p>
Your user name is: <b>[[user_name]]</b><br>
Your pass word is: <b>[[pass_word]]</b>
</";
$LANGARR['newMemberRegNotificationMailToUserSubject'] = "Welcome to Lingo Link";
$LANGARR['next'] = "Next >";
$LANGARR['noThanks'] = "no thanks";
$LANGARR['notifyByEmail'] = "Do you want to be notified of profile activity and lesson bookings at this email address?";
$LANGARR['of'] = "of";
$LANGARR['password'] = "Password";
$LANGARR['passwordError'] = "The password you have entered is invalid";
$LANGARR['passwordLengthError'] = "Password must be alpha-numeric and at least should have six characters";
$LANGARR['phone'] = "Phone";
$LANGARR['placesPlanToVisit'] = "Places you plan to visit";
$LANGARR['placesTravelled'] = "Places you have travelled";
$LANGARR['pleaseSelect'] = "Please Select";
$LANGARR['pollAlreadyEnrolled'] = "Thank you! You have already entered your answer";
$LANGARR['pollResult'] = "Result";
$LANGARR['pollTitle'] = "Poll Your Opinion";
$LANGARR['previous'] = "< Previous";
$LANGARR['print'] = "Print";
$LANGARR['readMore'] = "Read full story";
$LANGARR['recentArticles'] = "Recent Articles";
$LANGARR['regFormEmailNotes'] = "to complete your registration we will need to verify this";
$LANGARR['regionState'] = "State / Region";
$LANGARR['register'] = "Register";
$LANGARR['school'] = "School";
$LANGARR['signupForFree'] = "Sign-up for free";
$LANGARR['signupForNewsletter'] = "Sign-up for our newsletter
";
$LANGARR['subject'] = "Subject";
$LANGARR['submit'] = "Submit";
$LANGARR['subscribeToNewsletter'] = "Subscribe to newsletter";
$LANGARR['takeASiteTour'] = "Take a site tour";
$LANGARR['thanksTextAfterEnquiry'] = "<p>Thanks for contacting us. We will respond to your enquiry as soon as possible</p>

<p>
Please <a href=\"/\">click here</a> to return to the home page
</p>";
$LANGARR['thanksTextAfterNewMemberActivation'] = "<p>Thank you..</p>

<p>Your account has been successfully activated.</p>

<p>Please <a href=\"/eng/members/login/\">login</a> to access & edit your profile</p>

<p>Good luck..</p>";
$LANGARR['thanksTextAfterNewMemberRegistration'] = "<p>Thank you..</p>
<p>You have been registered successfully.</p>
<p>A notification email has been sent to the email address provided. To activate your account with us, you need to click the activation link in the email</p>
";
$LANGARR['title'] = "Title";
$LANGARR['to'] = "to";
$LANGARR['username'] = "Username";
$LANGARR['userNameError'] = "The user name you entered is invalid";
$LANGARR['usernameLengthError'] = "Username must be alpha-numeric and atleast should have six characters";
$LANGARR['webContact'] = "Web Contact";
$LANGARR['welcome'] = "Welcome";
$LANGARR['whatDoYouDo'] = "What do you do?";
$LANGARR['whatIsYourNativeLanguage'] = "What is your native language?";
$LANGARR['whatLangYouWantToLearn'] = "What languages do you want to learn (please indicate your level in each)?";
$LANGARR['whatLangYouWantToTeach'] = "What Languages can you teach?";
$LANGARR['yes'] = "yes";
$LANGARR['yourEmailAddress'] = "Your email address";

/*** FROM VALUE LIST TABLE ***/
$LANGARR['General Enquiry'] = "General Enquiry";
$LANGARR['Feedback'] = "Feedback";
$LANGARR['Technical Support'] = "Technical Support";
$LANGARR['Service Support'] = "Service Support";
$LANGARR['select 5'] = "";
$LANGARR['select 4'] = "";
$LANGARR['select 3'] = "";
$LANGARR['select 2'] = "";
$LANGARR['select 1'] = "";
$LANGARR['select 5'] = "";
$LANGARR['select 4'] = "";
$LANGARR['select 3'] = "";
$LANGARR['select 2'] = "";
$LANGARR['select 1'] = "";
$LANGARR['select 5'] = "";
$LANGARR['select 4'] = "";
$LANGARR['select 3'] = "";
$LANGARR['select 2'] = "";
$LANGARR['select 1'] = "";
$LANGARR['select 5'] = "";
$LANGARR['select 4'] = "";
$LANGARR['select 3'] = "";
$LANGARR['select 2'] = "";
$LANGARR['select 1'] = "";
$LANGARR['select 5'] = "";
$LANGARR['select 4'] = "";
$LANGARR['select 3'] = "";
$LANGARR['select 2'] = "";
$LANGARR['select 1'] = "";
$LANGARR['select 5'] = "";
$LANGARR['select 4'] = "";
$LANGARR['select 3'] = "";
$LANGARR['select 2'] = "";
$LANGARR['select 1'] = "";
$LANGARR['select 5'] = "";
$LANGARR['select 4'] = "";
$LANGARR['select 3'] = "";
$LANGARR['select 2'] = "";
$LANGARR['select 1'] = "";
$LANGARR['select 5'] = "";
$LANGARR['select 4'] = "";
$LANGARR['select 3'] = "";
$LANGARR['select 2'] = "";
$LANGARR['select 1'] = "";
$LANGARR['Advance'] = "Advance";
$LANGARR['Intermediate'] = "Intermediate";
$LANGARR['Beginner'] = "Beginner";
$LANGARR['Novice'] = "Novice";
$LANGARR['Abkhazian'] = "";
$LANGARR['Afar'] = "";
$LANGARR['Afrikaans'] = "";
$LANGARR['Akan'] = "";
$LANGARR['Albanian'] = "";
$LANGARR['Amharic'] = "";
$LANGARR['Arabic'] = "";
$LANGARR['Aragonese'] = "";
$LANGARR['Armenian'] = "";
$LANGARR['Assamese'] = "";
$LANGARR['Avaric'] = "";
$LANGARR['Avestan'] = "";
$LANGARR['Aymara'] = "";
$LANGARR['Azerbaijani'] = "";
$LANGARR['Bambara'] = "";
$LANGARR['Bashkir'] = "";
$LANGARR['Basque'] = "";
$LANGARR['Belarusian'] = "";
$LANGARR['Bengali'] = "";
$LANGARR['Bislama'] = "";
$LANGARR['Bosnian'] = "";
$LANGARR['Breton'] = "";
$LANGARR['Bulgarian'] = "";
$LANGARR['Burmese'] = "";
$LANGARR['Catalan'] = "";
$LANGARR['Chamorro'] = "";
$LANGARR['Chechen'] = "";
$LANGARR['Chichewa; Nyanja'] = "";
$LANGARR['Chinese'] = "";
$LANGARR['Chuvash'] = "";
$LANGARR['Cornish'] = "";
$LANGARR['Corsican'] = "";
$LANGARR['Cree'] = "";
$LANGARR['Croatian'] = "";
$LANGARR['Czech'] = "";
$LANGARR['Danish'] = "";
$LANGARR['Divehi'] = "";
$LANGARR['Dutch'] = "";
$LANGARR['Dzongkha'] = "";
$LANGARR['English'] = "";
$LANGARR['Esperanto'] = "";
$LANGARR['Estonian'] = "";
$LANGARR['Ewe'] = "";
$LANGARR['Faroese'] = "";
$LANGARR['Fijian'] = "";
$LANGARR['Finnish'] = "";
$LANGARR['French'] = "";
$LANGARR['Frisian'] = "";
$LANGARR['Fulah'] = "";
$LANGARR['Gaelic'] = "";
$LANGARR['Gallegan'] = "";
$LANGARR['Ganda'] = "";
$LANGARR['Georgian'] = "";
$LANGARR['German'] = "";
$LANGARR['Greek'] = "";
$LANGARR['Guarani'] = "";
$LANGARR['Gujarati'] = "";
$LANGARR['Haitian; Haitian Creole'] = "";
$LANGARR['Hausa'] = "";
$LANGARR['Hebrew'] = "";
$LANGARR['Herero'] = "";
$LANGARR['Hindi'] = "";
$LANGARR['Hiri Motu'] = "";
$LANGARR['Hungarian'] = "";
$LANGARR['Icelandic'] = "";
$LANGARR['Ido'] = "";
$LANGARR['Igbo'] = "";
$LANGARR['Indonesian'] = "";
$LANGARR['Interlingua'] = "";
$LANGARR['Interlingue'] = "";
$LANGARR['Inuktitut'] = "";
$LANGARR['Inupiaq'] = "";
$LANGARR['Irish'] = "";
$LANGARR['Italian'] = "";
$LANGARR['Japanese'] = "";
$LANGARR['Javanese'] = "";
$LANGARR['Kalaallisut'] = "";
$LANGARR['Kannada'] = "";
$LANGARR['Kanuri'] = "";
$LANGARR['Kashmiri'] = "";
$LANGARR['Kazakh'] = "";
$LANGARR['Khmer'] = "";
$LANGARR['Kikuyu'] = "";
$LANGARR['Kinyarwanda'] = "";
$LANGARR['Kirghiz'] = "";
$LANGARR['Komi'] = "";
$LANGARR['Kongo'] = "";
$LANGARR['Korean'] = "";
$LANGARR['Kuanyama'] = "";
$LANGARR['Kurdish'] = "";
$LANGARR['Lao'] = "";
$LANGARR['Latin'] = "";
$LANGARR['Latvian'] = "";
$LANGARR['Letzeburgesch'] = "";
$LANGARR['Limburgish'] = "";
$LANGARR['Lingala'] = "";
$LANGARR['Lithuanian'] = "";
$LANGARR['Luba-Katanga'] = "";
$LANGARR['Macedonian'] = "";
$LANGARR['Malagasy'] = "";
$LANGARR['Malay'] = "";
$LANGARR['Malayalam'] = "";
$LANGARR['Maltese'] = "";
$LANGARR['Manx'] = "";
$LANGARR['Maori'] = "";
$LANGARR['Marathi'] = "";
$LANGARR['Marshall'] = "";
$LANGARR['Moldavian'] = "";
$LANGARR['Mongolian'] = "";
$LANGARR['Nauru'] = "";
$LANGARR['Navajo'] = "";
$LANGARR['Ndebele'] = "";
$LANGARR['Ndebele'] = "";
$LANGARR['Ndonga'] = "";
$LANGARR['Nepali'] = "";
$LANGARR['Northern Sami'] = "";
$LANGARR['Norwegian'] = "";
$LANGARR['Norwegian Bokm�l'] = "";
$LANGARR['Norwegian Nynorsk'] = "";
$LANGARR['Occitan'] = "";
$LANGARR['Ojibwa'] = "";
$LANGARR['Oriya'] = "";
$LANGARR['Oromo'] = "";
$LANGARR['Ossetian; Ossetic'] = "";
$LANGARR['Pali'] = "";
$LANGARR['Panjabi'] = "";
$LANGARR['Persian'] = "";
$LANGARR['Polish'] = "";
$LANGARR['Portuguese'] = "";
$LANGARR['Pushto'] = "";
$LANGARR['Quechua'] = "";
$LANGARR['Raeto-Romance'] = "";
$LANGARR['Romanian'] = "";
$LANGARR['Rundi'] = "";
$LANGARR['Russian'] = "";
$LANGARR['Samoan'] = "";
$LANGARR['Sango'] = "";
$LANGARR['Sanskrit'] = "";
$LANGARR['Sardinian'] = "";
$LANGARR['Serbian'] = "";
$LANGARR['Serbo-Croatian'] = "";
$LANGARR['Shona'] = "";
$LANGARR['Simplified Chinese'] = "";
$LANGARR['Sindhi'] = "";
$LANGARR['Sinhalese'] = "";
$LANGARR['Slavic'] = "";
$LANGARR['Slovak'] = "";
$LANGARR['Slovenian'] = "";
$LANGARR['Somali'] = "";
$LANGARR['Sotho'] = "";
$LANGARR['Spanish'] = "";
$LANGARR['Sundanese'] = "";
$LANGARR['Swahili'] = "";
$LANGARR['Swati'] = "";
$LANGARR['Swedish'] = "";
$LANGARR['Tagalog'] = "";
$LANGARR['Tahitian'] = "";
$LANGARR['Tajik'] = "";
$LANGARR['Tamil'] = "";
$LANGARR['Tatar'] = "";
$LANGARR['Telugu'] = "";
$LANGARR['Thai'] = "";
$LANGARR['Tibetan'] = "";
$LANGARR['Tigrinya'] = "";
$LANGARR['Tonga'] = "";
$LANGARR['Traditional Chinese'] = "";
$LANGARR['Tsonga'] = "";
$LANGARR['Tswana'] = "";
$LANGARR['Turkish'] = "";
$LANGARR['Turkmen'] = "";
$LANGARR['Twi'] = "";
$LANGARR['Uighur'] = "";
$LANGARR['Ukrainian'] = "";
$LANGARR['Urdu'] = "";
$LANGARR['Uzbek'] = "";
$LANGARR['Venda'] = "";
$LANGARR['Vietnamese'] = "";
$LANGARR['Volap�k'] = "";
$LANGARR['Walloon'] = "";
$LANGARR['Welsh'] = "";
$LANGARR['Wolof'] = "";
$LANGARR['Xhosa'] = "";
$LANGARR['Yi'] = "";
$LANGARR['Yiddish'] = "";
$LANGARR['Zhuang'] = "";
$LANGARR['Zulu'] = "";
$LANGARR['Afghan'] = "";
$LANGARR['Albanian'] = "";
$LANGARR['Algerian'] = "";
$LANGARR['American'] = "";
$LANGARR['American'] = "";
$LANGARR['Andorran'] = "";
$LANGARR['Angolan'] = "";
$LANGARR['Antiguans'] = "";
$LANGARR['Argentinean'] = "";
$LANGARR['Armenian'] = "";
$LANGARR['Australian'] = "";
$LANGARR['Austrian'] = "";
$LANGARR['Azerbaijani'] = "";
$LANGARR['Bahamian'] = "";
$LANGARR['Bahraini'] = "";
$LANGARR['Bangladeshi'] = "";
$LANGARR['Barbadian'] = "";
$LANGARR['Batswana'] = "";
$LANGARR['Belarusian'] = "";
$LANGARR['Belgian'] = "";
$LANGARR['Belizean'] = "";
$LANGARR['Beninese'] = "";
$LANGARR['Bhutanese'] = "";
$LANGARR['Bolivian'] = "";
$LANGARR['Bosnian'] = "";
$LANGARR['Herzegovinian'] = "";
$LANGARR['Brazilian'] = "";
$LANGARR['British'] = "";
$LANGARR['Bruneian'] = "";
$LANGARR['Bulgarian'] = "";
$LANGARR['Burkinabe'] = "";
$LANGARR['Burmese'] = "";
$LANGARR['Burundian'] = "";
$LANGARR['Cambodian'] = "";
$LANGARR['Cameroonian'] = "";
$LANGARR['Canadian'] = "";
$LANGARR['Cape Verdian'] = "";
$LANGARR['Central African'] = "";
$LANGARR['Chadian'] = "";
$LANGARR['Chilean'] = "";
$LANGARR['Chinese'] = "";
$LANGARR['Colombian'] = "";
$LANGARR['Comoran'] = "";
$LANGARR['Congolese'] = "";
$LANGARR['Costa Rican'] = "";
$LANGARR['Croatian'] = "";
$LANGARR['Cuban'] = "";
$LANGARR['Cypriot'] = "";
$LANGARR['Czech'] = "";
$LANGARR['Danish'] = "";
$LANGARR['Djibouti'] = "";
$LANGARR['Dominican'] = "";
$LANGARR['Dominican'] = "";
$LANGARR['Dutch'] = "";
$LANGARR['Ecuadorean'] = "";
$LANGARR['Egyptian'] = "";
$LANGARR['Emirian'] = "";
$LANGARR['Equatorial Guinean'] = "";
$LANGARR['Eritrean'] = "";
$LANGARR['Estonian'] = "";
$LANGARR['Ethiopian'] = "";
$LANGARR['Fijian'] = "";
$LANGARR['Filipino'] = "";
$LANGARR['Finnish'] = "";
$LANGARR['French'] = "";
$LANGARR['Gabonese'] = "";
$LANGARR['Gambian'] = "";
$LANGARR['Georgian'] = "";
$LANGARR['German'] = "";
$LANGARR['Ghanaian'] = "";
$LANGARR['Greek'] = "";
$LANGARR['Grenadian'] = "";
$LANGARR['Guatemalan'] = "";
$LANGARR['Guinea-Bissauan'] = "";
$LANGARR['Guinean'] = "";
$LANGARR['Guyanese'] = "";
$LANGARR['Haitian'] = "";
$LANGARR['Honduran'] = "";
$LANGARR['Hungarian'] = "";
$LANGARR['Icelander'] = "";
$LANGARR['I-Kiribati'] = "";
$LANGARR['Indian'] = "";
$LANGARR['Indonesian'] = "";
$LANGARR['Iranian'] = "";
$LANGARR['Iraqi'] = "";
$LANGARR['Irish'] = "";
$LANGARR['Israeli'] = "";
$LANGARR['Italian'] = "";
$LANGARR['Ivorian'] = "";
$LANGARR['Jamaican'] = "";
$LANGARR['Japanese'] = "";
$LANGARR['Jordanian'] = "";
$LANGARR['Kazakhstani'] = "";
$LANGARR['Kenyan'] = "";
$LANGARR['Kirghiz'] = "";
$LANGARR['Kittian and Nevisian'] = "";
$LANGARR['Kuwaiti'] = "";
$LANGARR['Laotian'] = "";
$LANGARR['Latvian'] = "";
$LANGARR['Lebanese'] = "";
$LANGARR['Liberian'] = "";
$LANGARR['Libyan'] = "";
$LANGARR['Liechtensteiner'] = "";
$LANGARR['Lithuanian'] = "";
$LANGARR['Luxembourger'] = "";
$LANGARR['Macedonian'] = "";
$LANGARR['Malagasy'] = "";
$LANGARR['Malawian'] = "";
$LANGARR['Malaysian'] = "";
$LANGARR['Maldivan'] = "";
$LANGARR['Malian'] = "";
$LANGARR['Maltese'] = "";
$LANGARR['Marshallese'] = "";
$LANGARR['Mauritanian'] = "";
$LANGARR['Mauritian'] = "";
$LANGARR['Mexican'] = "";
$LANGARR['Micronesian'] = "";
$LANGARR['Moldovan'] = "";
$LANGARR['Monegasque'] = "";
$LANGARR['Mongolian'] = "";
$LANGARR['Moroccan'] = "";
$LANGARR['Mosotho'] = "";
$LANGARR['Mozambican'] = "";
$LANGARR['Namibian'] = "";
$LANGARR['Nauruan'] = "";
$LANGARR['Nepalese'] = "";
$LANGARR['New Zealander'] = "";
$LANGARR['Nicaraguan'] = "";
$LANGARR['Nigerian'] = "";
$LANGARR['Nigerien'] = "";
$LANGARR['Ni-Vanuatu'] = "";
$LANGARR['North Korean'] = "";
$LANGARR['Norwegian'] = "";
$LANGARR['Omani'] = "";
$LANGARR['Pakistani'] = "";
$LANGARR['Palauan'] = "";
$LANGARR['Palestinian'] = "";
$LANGARR['Panamanian'] = "";
$LANGARR['Papua New Guinean'] = "";
$LANGARR['Paraguayan'] = "";
$LANGARR['Peruvian'] = "";
$LANGARR['Polish'] = "";
$LANGARR['Portuguese'] = "";
$LANGARR['Qatari'] = "";
$LANGARR['Romanian'] = "";
$LANGARR['Russian'] = "";
$LANGARR['Rwandan'] = "";
$LANGARR['Saint Lucian'] = "";
$LANGARR['Salvadoran'] = "";
$LANGARR['Sammarinese'] = "";
$LANGARR['Samoan'] = "";
$LANGARR['Sao Tomean'] = "";
$LANGARR['Saudi Arabian'] = "";
$LANGARR['Senegalese'] = "";
$LANGARR['Serbian'] = "";
$LANGARR['Seychellois'] = "";
$LANGARR['Sierra Leonean'] = "";
$LANGARR['Singaporean'] = "";
$LANGARR['Slovak'] = "";
$LANGARR['Slovene'] = "";
$LANGARR['Solomon Islander'] = "";
$LANGARR['Somali'] = "";
$LANGARR['South African'] = "";
$LANGARR['South Korean'] = "";
$LANGARR['Spanish'] = "";
$LANGARR['Sri Lankan'] = "";
$LANGARR['Sudanese'] = "";
$LANGARR['Surinamer'] = "";
$LANGARR['Swazi'] = "";
$LANGARR['Swedish'] = "";
$LANGARR['Swiss'] = "";
$LANGARR['Syrian'] = "";
$LANGARR['Tadzhik'] = "";
$LANGARR['Taiwanese'] = "";
$LANGARR['Tanzanian'] = "";
$LANGARR['Thai'] = "";
$LANGARR['Togolese'] = "";
$LANGARR['Tongan'] = "";
$LANGARR['Trinidadian'] = "";
$LANGARR['Tunisian'] = "";
$LANGARR['Turkish'] = "";
$LANGARR['Turkmen'] = "";
$LANGARR['Tuvaluan'] = "";
$LANGARR['Ugandan'] = "";
$LANGARR['Ukrainian'] = "";
$LANGARR['Uruguayan'] = "";
$LANGARR['Uzbekistani'] = "";
$LANGARR['Venezuelan'] = "";
$LANGARR['Vietnamese'] = "";
$LANGARR['Yemeni'] = "";
$LANGARR['Zambian'] = "";
$LANGARR['Zimbabwean'] = "";
$LANGARR['English'] = "";
$LANGARR['中文'] = "";
$LANGARR['日本語'] = "";

?>